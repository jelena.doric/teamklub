import React from 'react';
import { createBrowserHistory } from 'history';
import { ThemeProvider } from '@material-ui/styles';
import './App.css';
import theme from './theme';
import 'react-perfect-scrollbar/dist/css/styles.css';
import AuthenticatedRoutes from "./AuthenticatedRoutes";
import Login from "./components/Login";
import Register from "./components/Register";
import { Router as Router, Route, Switch } from 'react-router-dom'
import {RouteWithLayout} from "./components";
import {Minimal as MinimalLayout} from "./layouts";

function TeamKlubApp() {
  const browserHistory = createBrowserHistory();

  return (
    <ThemeProvider theme={theme}>
        <Router history={browserHistory}>
          <Switch>
            <RouteWithLayout
              component={Login}
              exact
              layout={MinimalLayout}
              path="/signin"
            />
            <RouteWithLayout
              component={Register}
              exact
              layout={MinimalLayout}
              path="/signup"
            />
            <AuthenticatedRoutes/>
          </Switch>
        </Router>
    </ThemeProvider>
  );
}

export default TeamKlubApp;
