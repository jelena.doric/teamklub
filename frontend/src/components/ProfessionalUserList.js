import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import ProfessionalUser from './ProfessionalUser';
import axios from 'axios';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { makeStyles } from '@material-ui/core/styles';
import AuthenticationService from '../service/AuthenticationService';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

function ProfessionalUserList(props) {
  const classes = useStyles();
  const [users, setUsers] = React.useState([]);
  const [categories, setCategories] = React.useState([]);
  const [category, setCategory] = React.useState({});
  const { history } = props.history;

  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: AuthenticationService.getToken()
    }
  };

  React.useEffect(() => {
    fetch('https://immense-harbor-53286.herokuapp.com/app-users/professionals', options)
      .then(data => data.json())
      .then(users => setUsers(users));
  }, []);

  React.useEffect(() => {
    fetch('https://immense-harbor-53286.herokuapp.com/categories', options)
      .then(data => data.json())
      .then(categories => setCategories(categories));
  }, []);

  function onChange(event) {
    setCategory(event.target.value);
  }

  return (
    <div>
      {users ? (
        <div>
          <FormControl className={classes.formControl}>
            <InputLabel>Search Category</InputLabel>
            <Select
              value={category === undefined ? 'All' : category}
              name="category"
              onChange={onChange}
              id={'searchInput'}
              allign={'center'}>
              {categories.map(category => (
                <MenuItem value={category} key={category.id}>
                  <em>{category.categoryName}</em>
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <Grid container spacing={1} style={{ padding: 24 }}>
            {users
              .filter(user =>
                category === undefined || category.categoryName === undefined
                  ? true
                  : user.category.categoryName.toLowerCase() ===
                    category.categoryName.toLowerCase()
              )
              .map(currentUser => (
                <Grid item xs={12} sm={6} lg={4} xl={3} key={currentUser.id}>
                  <ProfessionalUser
                    user={currentUser}
                    history={props.history}
                  />
                </Grid>
              ))}
          </Grid>
        </div>
      ) : (
        'Loading...'
      )}
    </div>
  );
}

export default ProfessionalUserList;
