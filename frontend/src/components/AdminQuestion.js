import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { CardHeader, Link } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import CardActionArea from '@material-ui/core/CardActionArea';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink, withRouter } from 'react-router-dom';

//Rjeseno po uzoru na Questions -> QuestionList -> YourQuestions
const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

const Question = props => {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
    <CardHeader
      title={
        <Typography className={classes.title} color="textSecondary">
          Asked by {' '}
          <Link
          href="#"
          variant="body2"
          component={RouterLink}
          to={{
            pathname: '/userprofilepage',
              state: {
                user: props.question.sender
              }
          }}>
          {props.question.sender.username}
          </Link>{' '}
        </Typography>
      }
    />
  <CardContent>
    <Typography gutterBottom variant="h5" component="h2">
      {props.question.messageText}
    </Typography>
    </CardContent>
  </Card>
);
};

export default Question;
