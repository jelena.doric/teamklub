import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { CardHeader, Link } from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink, withRouter } from 'react-router-dom';

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

const ProfessionalUser = props => {
  const { history } = props;
  const classes = useStyles();
  const skills = props.user.skills;
  const qualifications = props.user.qualifications;
  const skillsList = [];
  const qualificationsList = [];

  for (let i = 0, l = props.user.skills.length; i < l; i += 1) {
    if (skills[i].skillName) {
      skillsList.push(skills[i].skillName);
    }
  }

  for (let i = 0, l = props.user.qualifications.length; i < l; i += 1) {
    if (qualifications[i].qualificationName) {
      qualificationsList.push(qualifications[i].qualificationName);
    }
  }

  function askPro(e) {
    e.preventDefault();
    history.push({
      pathname: '/askrequest',
      state: {
        category: props.user.category,
        professionalId: props.user.id
      }
    });
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        title={
          <Typography className={classes.title} color="textSecondary">
            username:{' '}
            <Link
              href="#"
              variant="body2"
              component={RouterLink}
              to={{
                pathname: '/userprofilepage',
                state: {
                  user: props.user
                }
              }}>
              {props.user.username}{' '}
            </Link>
            <br />
            email: {props.user.email} <br />
            rating: {props.user.rating == null ? 'None' : props.user.rating}
          </Typography>
        }
      />
      <CardContent>
        <Typography variant="body3" component="p">
          qualifications:{' '}
          {qualificationsList.map(qual => (
            <li>{qual}</li>
          ))}{' '}
          <br />
          skills:{' '}
          {skillsList.map(skill => (
            <li>{skill}</li>
          ))}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          color="primary"
          variant="outlined"
          onClick={askPro}>
          Ask For Help
        </Button>
      </CardActions>
    </Card>
  );
};
export default ProfessionalUser;
