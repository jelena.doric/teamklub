import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import AdminQuestion from './AdminQuestion';
import axios from 'axios';
import AuthenticationService from 'service/AuthenticationService';
class AdminQuestionList extends Component {
  state = {
    services: [],
    searchString: ''
  };

  constructor() {
    super();
    this.getQuestions();
  }

  async getQuestions() {
    const url = 'https://immense-harbor-53286.herokuapp.com/messages';
    axios.get(url).then(res => {
      this.setState({ questions: res.data });
    });
  }

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };

  onDelete = id => {
    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };

    fetch('https://immense-harbor-53286.herokuapp.com/messages/' + id, options).then(() =>
      this.getQuestions()
    );
  };

  render() {
    return (
      <div>
        {this.state.questions ? (
          <div>
            <TextField
              style={{ padding: 24 }}
              id={'searchInput'}
              placeholder={'Search questions by text'}
              margin={'normal'}
              inputProps={{ min: 0, style: { textAlign: 'center' } }}
              onChange={this.onSearchInputChange}
              allign={'center'}
            />
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.questions
                .filter(question =>
                  question.messageText
                    .toLowerCase()
                    .includes(this.state.searchString.toLowerCase())
                )
                .map(currentQuestion => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={currentQuestion.id}>
                    <AdminQuestion question={currentQuestion} />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default AdminQuestionList;
