import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { CardHeader, Link } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import CardActionArea from '@material-ui/core/CardActionArea';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import AuthenticationService from 'service/AuthenticationService';

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

const ResumeOverview = props => {
  async function deleteResume(e) {
    e.preventDefault();
    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };

    fetch(
      'https://immense-harbor-53286.herokuapp.com/resumes/' + props.resume.id,
      options
    ).then(() => window.location.reload());
  }

  async function acceptResume(e) {
    e.preventDefault();
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      },
      body: JSON.stringify(props.resume)
    };
    const optionsForDelete = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };
    fetch(
      'https://immense-harbor-53286.herokuapp.com/app-users/professional/' + props.resume.user.id,
      options
    ).then(() =>
      fetch(
        'https://immense-harbor-53286.herokuapp.com/resumes/' + props.resume.id,
        optionsForDelete
      ).then(() => window.location.reload())
    );
  }

  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardHeader
        title={
          <Typography className={classes.title} color="textSecondary">
            Asked by{' '}
            <Link
              href="#"
              variant="body2"
              component={RouterLink}
              to={{
                pathname: '/userprofilepage',
                state: {
                  user: props.resume.user
                }
              }}>
              {props.resume.user.username}
            </Link>{' '}
          </Typography>
        }
      />
      <CardContent>
        <Typography variant="body3" component="p">
          qualifications:{' '}
          {props.resume.qualifications.map(qual => (
            <li>{qual.qualificationName}</li>
          ))}{' '}
          <br />
          skills:{' '}
          {props.resume.skills.map(skill => (
            <li>{skill.skillName}</li>
          ))}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          color="primary"
          variant="outlined"
          onClick={deleteResume}>
          Delete resume
        </Button>
        <Button
          size="small"
          color="primary"
          variant="outlined"
          onClick={acceptResume}>
          Accept resume
        </Button>
      </CardActions>
    </Card>
  );
};

export default ResumeOverview;
