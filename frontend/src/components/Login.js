import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import AuthenticationService from '../service/AuthenticationService';
import axios from 'axios';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'TeamKlub '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  error: {
    color: 'red'
  }
}));

function Login(props) {
  const classes = useStyles();
  const [loginForm, setLoginForm] = React.useState({
    username: '',
    password: ''
  });
  const { history } = props;

  const [error, setError] = React.useState('');

  function onChange(event) {
    const { name, value } = event.target;
    setLoginForm(oldForm => ({ ...oldForm, [name]: value }));
  }

  if (AuthenticationService.isUserLoggedIn()) {
    AuthenticationService.logout();
  }

  function onLogin(e) {
    e.preventDefault();

    AuthenticationService.executeJwtAuthenticationService(
      loginForm.username,
      loginForm.password
    )
      .then(response => {
        AuthenticationService.registerSuccessfulLoginForJwt(
          loginForm.username,
          response.data.token
        );
        AuthenticationService.registerSessionUser(response.data.user);
        history.push({ pathname: '/homepage' });
      })
      .catch(() => {
        setError('Incorrect username or password');
      });
  }

  function isValid() {
    const { username, password } = loginForm;
    return username.length > 0 && password.length > 0;
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={onLogin}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            onChange={onChange}
            value={loginForm.username}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={onChange}
            value={loginForm.password}
          />
          <Grid className={classes.error}>{error}</Grid>
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}
            disabled={!isValid()}>
            Sign In
          </Button>
          <Link href="#" variant="body2" component={RouterLink} to="/signup">
            {"Don't have an account? Sign Up"}
          </Link>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

//export default Login;
export default withRouter(Login);
