import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Answer from './Answer';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AuthenticationService from 'service/AuthenticationService';

class AnswerList extends Component {
  state = {
    services: [],
    searchString: ''
  };

  constructor(props) {
    super(props);
    this.getAnswers(props);
  }

  async getAnswers(props) {
    const url =
      'https://immense-harbor-53286.herokuapp.com/questions/' +
      this.props.history.location.state.id +
      '/answers';
    axios.get(url).then(res => this.setState({ answers: res.data }));
  }

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };

  GoAnswer = event => {
    event.preventDefault();
    this.props.history.push({
      pathname: '/goanswer',
      state: {
        id: this.props.history.location.state.id,
        questionText: this.props.history.location.state.questionText
      }
    });
  };

  render() {
    return (
      <div>
        {this.state.answers ? (
          <div>
            <Grid container spacing={2} style={{ padding: 24 }}>
              <Typography component="h1" variant="h5">
                {this.props.history.location.state.questionText}
              </Typography>
            </Grid>
            <Grid container spacing={2} style={{ padding: 24 }}>
              <TextField
                id={'searchInput'}
                placeholder={'Search answers'}
                margin={'normal'}
                inputProps={{ min: 0, style: { textAlign: 'center' } }}
                onChange={this.onSearchInputChange}
              />
            </Grid>
            <Grid container spacing={2} style={{ padding: 24 }}>
              <Button
                margin={'normal'}
                color="primary"
                variant="outlined"
                onClick={this.GoAnswer}>
                Answer
              </Button>
            </Grid>
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.answers
                .filter(answer =>
                  answer.answerTXT
                    .toLowerCase()
                    .includes(this.state.searchString.toLowerCase())
                )
                .map(currentAnswer => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={currentAnswer.id}>
                    <Answer
                      answer={currentAnswer}
                      history={this.props.history}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default AnswerList;
