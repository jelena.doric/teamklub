import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import PendingRequest from './PendingRequest';
import axios from 'axios';

class PendingRequestList extends Component {
  state = {
    requests: [],
    searchString: ''
  };

  constructor() {
    super();
    this.getRequests();
  }

  async getRequests() {
    const url = 'https://immense-harbor-53286.herokuapp.com/app-users/requests/answered/3';
    axios.get(url).then(res => {
      this.setState({ requests: res.data });
    });
  }

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };

  render() {
    return (
      <div>
        {this.state.requests ? (
          <div>
            <TextField
              style={{ padding: 24 }}
              id={'searchInput'}
              placeholder={'Search requests'}
              margin={'normal'}
              inputProps={{ min: 0, style: { textAlign: 'center' } }}
              onChange={this.onSearchInputChange}
              allign={'center'}
            />
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.requests
                .filter(request =>
                  request.requestMessage
                    .toLowerCase()
                    .includes(this.state.searchString.toLowerCase())
                )
                .map(currentRequest => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={currentRequest.id}>
                    <PendingRequest
                      request={currentRequest}
                      history={this.props.history}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default PendingRequestList;
