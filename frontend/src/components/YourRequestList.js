import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Request from './Request';
import axios from 'axios';
import AuthenticationService from 'service/AuthenticationService';

class YourRequestList extends Component {
  state = {
    requests: [],
    searchString: '',
    user: AuthenticationService.getLoggedInUser()
  };

  constructor() {
    super();
    this.getRequests();
  }

  async getRequests() {
    const url =
      'https://immense-harbor-53286.herokuapp.com/app-users/requests/asked/' + this.state.user.id;
    axios.get(url).then(res => {
      this.setState({ requests: res.data });
    });
  }

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };

  onDelete = id => {
    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };
    fetch(
      'https://immense-harbor-53286.herokuapp.com/app-users/requests/asked/' + id,
      options
    ).then(() => this.getRequests());
  };

  render() {
    return (
      <div>
        {this.state.requests ? (
          <div>
            <TextField
              style={{ padding: 24 }}
              id={'searchInput'}
              placeholder={'Search requests'}
              margin={'normal'}
              inputProps={{ min: 0, style: { textAlign: 'center' } }}
              onChange={this.onSearchInputChange}
              allign={'center'}
            />
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.requests
                .filter(request =>
                  request.category == null
                    ? true
                    : request.category.categoryName
                        .toLowerCase()
                        .includes(this.state.searchString.toLowerCase())
                )
                .map(currentRequest => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={currentRequest.id}>
                    <Request
                      onDelete={this.onDelete}
                      request={currentRequest}
                      history={this.props.history}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default YourRequestList;
