import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import ResumeOverview from './ResumeOverview';
import axios from 'axios';
import AuthenticationService from 'service/AuthenticationService';

class ResumeList extends Component {
  state = {
    services: [],
    resumes: [],
    searchString: ''
  };

  constructor() {
    super();
    this.getResumes();
  }

  async getResumes() {
    const url = 'https://immense-harbor-53286.herokuapp.com/resumes';
    axios.get(url).then(res => {
      this.setState({ resumes: res.data });
    });
  }

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };

  onDelete = id => {
    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };

    fetch('https://immense-harbor-53286.herokuapp.com/resumes/' + id, options).then(() =>
      this.getResumes()
    );
  };

  render() {
    return (
      <div>
        {this.state.resumes ? (
          <div>
            <TextField
              style={{ padding: 24 }}
              id={'searchInput'}
              placeholder={'Search resumes'}
              margin={'normal'}
              inputProps={{ min: 0, style: { textAlign: 'center' } }}
              onChange={this.onSearchInputChange}
              allign={'center'}
            />
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.resumes
                .filter(resume =>
                  resume.user.username
                    .toLowerCase()
                    .includes(this.state.searchString.toLowerCase())
                )
                .map(currentResume => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={currentResume.id}>
                    <ResumeOverview
                      onDelete={this.onDelete}
                      resume={currentResume}
                      history={this.props.history}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default ResumeList;
