import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import User from './User';
import axios from 'axios';
import AuthenticationService from 'service/AuthenticationService';

class UserList extends Component {
  state = {
    services: [],
    searchString: ''
  };

  constructor() {
    super();
    this.getUsers();
  }

  async getUsers() {
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };
    const url = 'https://immense-harbor-53286.herokuapp.com/app-users';
    fetch(url, options)
      .then(e => e.json())
      .then(res => {
        this.setState({ users: res });
      });
  }

  onDelete = id => {
    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };

    fetch('https://immense-harbor-53286.herokuapp.com/app-users/' + id, options).then(() =>
      this.getUsers()
    );
  };

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };

  render() {
    return (
      <div>
        {this.state.users ? (
          <div>
            <TextField
              style={{ padding: 24 }}
              id={'searchInput'}
              placeholder={'Search users'}
              margin={'normal'}
              inputProps={{ min: 0, style: { textAlign: 'center' } }}
              onChange={this.onSearchInputChange}
              allign={'center'}
            />
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.users
                .filter(user =>
                  user.username
                    .toLowerCase()
                    .includes(this.state.searchString.toLowerCase())
                )
                .map(currentUser => (
                  <Grid item xs={12} sm={6} lg={4} xl={3} key={currentUser.id}>
                    <User
                      user={currentUser}
                      history={this.props.history}
                      onDelete={this.onDelete}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default UserList;
