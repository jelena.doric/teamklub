import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { CardHeader, Link } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import CardActionArea from '@material-ui/core/CardActionArea';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

const PendingRequest = props => {

  function GoAnswerRequest(e) {
    e.preventDefault();
    props.history.push({
      pathname: '/goanswerrequest',
      id: props.request.id
    });
  }

  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardHeader
        title={
          <Typography className={classes.title} color="textSecondary">
            Asked by {props.request.user.username} In category{' '}
            {props.request.category.categoryName}
          </Typography>
        }
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {props.request.requestMessage}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          color="primary"
          variant="outlined"
          onClick={GoAnswerRequest}>
          Answer
        </Button>
      </CardActions>
    </Card>
  );
};

export default PendingRequest;
