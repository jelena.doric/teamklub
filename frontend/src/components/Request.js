import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { CardHeader, Link } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import CardActionArea from '@material-ui/core/CardActionArea';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink, withRouter } from 'react-router-dom';

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

const Request = props => {
  function ViewAnswers(e) {
    e.preventDefault();
    props.history.push({
      pathname: '/requestanswers',
      id: props.request.id
    });
  }

  function GoAnswerRequest(e) {
    e.preventDefault();
    props.history.push({
      pathname: '/goanswerrequest',
      id: props.request.id
    });
  }

  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardHeader
        title={
          <Typography className={classes.title} color="textSecondary">
            You asked{' '}
            <Link
              href="#"
              variant="body2"
              component={RouterLink}
              to={{
                pathname: '/userprofilepage',
                state: {
                  user: props.request.user
                }
              }}>
              {props.request.user.username}
            </Link>{' '}
            In category{' '}
            {props.request.category == null
              ? true
              : props.request.category.categoryName}
          </Typography>
        }
        action={
          <IconButton
            onClick={props.onDelete.bind(this, props.request.id)}
            aria-label="delete">
            <CancelIcon />
          </IconButton>
        }
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {props.request.requestMessage}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          color="primary"
          variant="outlined"
          onClick={ViewAnswers}>
          View Answers
        </Button>
      </CardActions>
    </Card>
  );
};

export default Request;
