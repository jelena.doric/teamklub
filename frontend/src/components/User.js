import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { CardHeader, Link } from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import AuthenticationService from 'service/AuthenticationService';

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
    colour: 'red'
  },
  media: {
    height: 140
  }
});

const User = props => {
  const classes = useStyles();

  function grantAdmin() {
    const id = props.user.id;
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };
    fetch('https://immense-harbor-53286.herokuapp.com/app-users/admin/rights/' + id, options).then(() =>
      window.location.reload()
    );
  }

  function banUser() {
    const id = props.user.id;
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      }
    };
    fetch('https://immense-harbor-53286.herokuapp.com/app-users/ban/' + id, options).then(() =>
      window.location.reload()
    );
  }
  if (props.user.timeout === null) {
    return (
      <Card className={classes.card}>
        <CardHeader
          title={
            <Typography className={classes.title} color="textSecondary">
              username: {props.user.username} <br /> email: {props.user.email}
            </Typography>
          }
        />
        <CardActions>
          <Button
            size="small"
            color="primary"
            variant="outlined"
            onClick={banUser}>
            Ban User
          </Button>
          <Button
            size="small"
            variant="outlined"
            color="inherit"
            onClick={props.onDelete.bind(this, props.user.id)}>
            Deactivate User
          </Button>
          <Button
            size="small"
            variant="outlined"
            color="inherit"
            onClick={grantAdmin}>
            Grant admin rights
          </Button>
        </CardActions>
      </Card>
    );
  } else {
    return (
      <Card className={classes.card}>
        <CardHeader
          title={
            <Typography className={classes.title} color="textSecondary">
              BANNED <br />
              username: {props.user.username} <br /> email: {props.user.email}
            </Typography>
          }
        />
        <CardActions>
          <Button
            size="small"
            variant="outlined"
            color="inherit"
            onClick={props.onDelete.bind(this, props.user.id)}>
            Deactivate User
          </Button>
        </CardActions>
      </Card>
    );
  }
};
export default User;
