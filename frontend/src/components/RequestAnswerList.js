import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import RequestAnswer from './RequestAnswer';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import AuthenticationService from '../service/AuthenticationService';

class RequestAnswerList extends Component {
  state = {
    services: [],
    searchString: ''
  };

  constructor(props) {
    super(props);
    const user = AuthenticationService.getLoggedInUser();
    console.log(user);
    this.getAnswers(props);
  }

  async getAnswers(props) {
    const url = 'https://immense-harbor-53286.herokuapp.com/requests/' + props.id + '/answers';
    axios.get(url).then(res => {
      this.setState({ answers: res.data });
    });
  }

  ratePro = e => {
    e.preventDefault();
    this.props.history.push({
      pathname: '/rateprofessional'
    });
  };

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };

  render() {
    return (
      <div>
        {this.state.answers ? (
          <div>
            <TextField
              style={{ padding: 24 }}
              id={'searchInput'}
              placeholder={'Search answers'}
              margin={'normal'}
              inputProps={{ min: 0, style: { textAlign: 'center' } }}
              onChange={this.onSearchInputChange}
              allign={'center'}
            />
            <Button variant="outlined" color="primary" onClick={this.ratePro}>
              Rate Professional
            </Button>
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.answers
                .filter(answer =>
                  answer.answerTxt
                    .toLowerCase()
                    .includes(this.state.searchString.toLowerCase())
                )
                .map(currentAnswer => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={currentAnswer.id}>
                    <RequestAnswer
                      answer={currentAnswer}
                      history={this.props.history}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default RequestAnswerList;
