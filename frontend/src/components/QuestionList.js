import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Question from './Question';
import axios from 'axios';
import AuthenticationService from 'service/AuthenticationService';
import { useRadioGroup } from '@material-ui/core';

class QuestionList extends Component {
  state = {
    questions: [],
    searchString: '',
    user: AuthenticationService.getLoggedInUser()
  };

  constructor(props) {
    super(props);
    this.getQuestions();
    console.log(props);
  }

  async getQuestions() {
    axios
      .get('https://immense-harbor-53286.herokuapp.com/questions')
      .then(res => {
        this.setState({ questions: res.data });
      });
  }

  onSearchInputChange = event => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: '' });
    }
  };
  onDelete = id => {
    if (this.state.user.role.roleName === 'ROLE_ADMIN') {
      axios
        .delete('https://immense-harbor-53286.herokuapp.com/questions/' + id)
        .then(() => this.getQuestions());
    }
  };

  render() {
    return (
      <div>
        {this.state.questions ? (
          <div>
            <TextField
              style={{ padding: 24 }}
              id={'searchInput'}
              placeholder={'Search questions'}
              margin={'normal'}
              inputProps={{ min: 0, style: { textAlign: 'center' } }}
              onChange={this.onSearchInputChange}
              allign={'center'}
            />
            <Grid container spacing={1} style={{ padding: 24 }}>
              {this.state.questions
                .filter(question =>
                  question.category.categoryName
                    .toLowerCase()
                    .includes(this.state.searchString.toLowerCase())
                )
                .map(currentQuestion => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={currentQuestion.id}>
                    <Question
                      onDelete={this.onDelete}
                      question={currentQuestion}
                      history={this.props.history}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        ) : (
          'Loading...'
        )}
      </div>
    );
  }
}

export default QuestionList;
