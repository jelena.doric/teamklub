import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Link as RouterLink } from 'react-router-dom';
import AuthenticationService from '../service/AuthenticationService';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'TeamKlub '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  error: {
    color: 'red'
  },
  success: {
    color: 'green'
  }
}));

export default function Register(props) {
  const classes = useStyles();
  const [form, setForm] = React.useState({
    firstName: '',
    lastName: '',
    userName: '',
    email: '',
    userPassword: ''
  });
  const [error, setError] = React.useState('');
  const [success, setSuccess] = React.useState('');

  function onChange(event) {
    const { name, value } = event.target;
    setForm(oldForm => ({ ...oldForm, [name]: value }));
  }

  if (AuthenticationService.isUserLoggedIn()) {
    AuthenticationService.logout();
  }

  function onSubmit(e) {
    e.preventDefault();
    setError('');
    setSuccess('');
    const data = {
      firstName: form.firstName,
      lastName: form.lastName,
      username: form.userName,
      email: form.email,
      password: form.userPassword
    };
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
    fetch('https://immense-harbor-53286.herokuapp.com/app-users', options).then(response => {
      if (response.status !== 201) {
        setError('username or email already taken');
      } else {
        setSuccess('Register success');
      }
    });
  }

  function isValid() {
    const { firstName, lastName, userName, email, userPassword } = form;
    return (
      email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) &&
      firstName.length > 0 &&
      lastName.length > 0 &&
      userName.length > 4 &&
      userName.length < 16 &&
      email.length > 4 &&
      email.length < 51 &&
      userPassword.length > 5 &&
      userPassword.length < 31
    );
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
                onChange={onChange}
                value={form.firstName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="lname"
                onChange={onChange}
                value={form.lastName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="userName"
                label="Username"
                name="userName"
                autoComplete="username"
                onChange={onChange}
                value={form.userName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={onChange}
                value={form.email}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="userPassword"
                label="Password"
                type="password"
                id="userPassword"
                autoComplete="current-password"
                onChange={onChange}
                value={form.userPassword}
              />
            </Grid>
            <Grid className={classes.error}>{error}</Grid>
            <Grid className={classes.success}>{success}</Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}
            disabled={!isValid()}>
            Sign Up
          </Button>
          <Grid container>
            <Grid item>
              <Link
                href="#"
                variant="body2"
                component={RouterLink}
                to="/signin">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
