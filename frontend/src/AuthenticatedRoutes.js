import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Routes from './Routes'
import AuthenticationService from './service/AuthenticationService';

class AuthenticatedRoutes extends Component {
  render() {
    if (AuthenticationService.isUserLoggedIn()) {
      return <Routes/>
    } else {
      return <Redirect to="/signin" />
    }
  }
}

export default AuthenticatedRoutes
