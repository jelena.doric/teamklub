import React from 'react';
import './App.css';
import 'react-perfect-scrollbar/dist/css/styles.css';
import TeamKlubApp from './TeamKlubApp';

function App() {
  return <TeamKlubApp/>;
}

export default App;
