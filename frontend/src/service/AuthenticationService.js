import axios from 'axios';

const API_URL = 'https://immense-harbor-53286.herokuapp.com';

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
export const SESSION_USER = 'sessionUser';
export const USER_SESSION_TOKEN = 'token';

class AuthenticationService {
  executeJwtAuthenticationService(username, password) {
    return axios.post(`${API_URL}/authenticate`, {
      username,
      password
    });
  }

  registerSuccessfulLoginForJwt(username, token) {
    const jwtToken = this.createJwtToken(token);
    this.setupAxiosInterceptors(jwtToken);
    sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
    sessionStorage.setItem(USER_SESSION_TOKEN, jwtToken);
  }

  registerSessionUser(user) {
    sessionStorage.setItem(SESSION_USER, JSON.stringify(user));
  }

  createJwtToken(token) {
    return 'Bearer ' + token;
  }

  logout() {
    sessionStorage.clear();
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    return user !== null;
  }

  getLoggedInUserName() {
    let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) return '';
    return user;
  }

  getLoggedInUser() {
    return JSON.parse(sessionStorage.getItem(SESSION_USER));
  }

  getToken() {
    return sessionStorage.getItem(USER_SESSION_TOKEN);
  }

  setupAxiosInterceptors(token) {
    axios.interceptors.request.use(config => {
      console.log('pozvao interceptor');
      if (this.isUserLoggedIn()) {
        config.headers.authorization = token;
      }
      return config;
    });
  }
}

export default new AuthenticationService();
