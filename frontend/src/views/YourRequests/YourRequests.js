import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import YourRequestList from 'components/YourRequestList';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const YourRequests = props => {
  const classes = useStyles();
  const { history } = props;

  return (
    <div className={classes.root}>
      {' '}
      <div>
        <Grid>
          <YourRequestList history={history} />
        </Grid>
      </div>
    </div>
  );
};

export default YourRequests;
