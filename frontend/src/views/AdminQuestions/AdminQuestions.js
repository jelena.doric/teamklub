import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import AdminQuestionList from 'components/AdminQuestionList';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const AdminQuestions = props => {
  const classes = useStyles();
  const { history } = props;

  return (
    <div className={classes.root}>
    {' '}
    <div>
    <Grid>
    <AdminQuestionList history={history} />
  </Grid>
  </div>
  </div>
);
};

export default AdminQuestions;
