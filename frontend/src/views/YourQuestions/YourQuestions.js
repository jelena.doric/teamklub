import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import YourQuestionList from 'components/YourQuestionList';
import AuthenticationService from 'service/AuthenticationService';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const YourQuestions = props => {
  const classes = useStyles();
  const { history } = props;
  const user = AuthenticationService.getLoggedInUser();
  console.log(user);
  return (
    <div className={classes.root}>
      {' '}
      <div>
        <Grid>
          <YourQuestionList history={history} />
        </Grid>
      </div>
    </div>
  );
};

export default YourQuestions;
