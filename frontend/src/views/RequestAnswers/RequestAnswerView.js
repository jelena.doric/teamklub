import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import RequestAnswerList from 'components/RequestAnswerList';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const RequestAnswers = props => {
  const classes = useStyles();

  const { history } = props;

  return (
    <div className={classes.root}>
      {' '}
      <div>
        <Grid>
          <RequestAnswerList history={history} id={props.location.id} />
        </Grid>
      </div>
    </div>
  );
};

export default RequestAnswers;
