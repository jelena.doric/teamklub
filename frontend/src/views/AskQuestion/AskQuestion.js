import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Avatar from '@material-ui/core/Avatar';
import HelpIcon from '@material-ui/icons/Help';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import Popup from 'reactjs-popup';
import { Map as LeafletMap, Marker, TileLayer } from 'react-leaflet';
import AuthenticationService from 'service/AuthenticationService';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(2, 0, 1)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  mapid: {
    height: '180px'
  },
  success: {
    pading: 24
  }
}));

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'TeamKlub '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function Dropdown(props) {
  const user = AuthenticationService.getLoggedInUser();
  const classes = useStyles();
  const { history } = props;
  const [form, setForm] = React.useState({
    questionText: '',
    category: {}
  });
  const [error, setError] = React.useState('');
  const [success, setSuccess] = React.useState('');
  const [categories, setCategories] = React.useState([]);
  const [value, setValue] = React.useState(true);
  const [coords, setCoords] = React.useState({
    longitude: 15.970947,
    latitude: 45.800434
  });
  console.log(coords);
  const MySwitch = ({ isOn, handleToggle, onSwitch }) => {
    return <Switch checked={isOn} onChange={handleToggle} onClick={onSwitch} />;
  };

  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: AuthenticationService.getToken()
    }
  };

  React.useEffect(() => {
    fetch('https://immense-harbor-53286.herokuapp.com/categories', options)
      .then(data => data.json())
      .then(categories => setCategories(categories));
  }, []);

  function onChange(event) {
    const { name, value } = event.target;
    setForm(oldForm => ({ ...oldForm, [name]: value }));
  }

  function onClick() {
    setValue(false);
  }

  function onSwitch(event) {
    console.log('Uspjeh');
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position =>
        setCoords(position.coords)
      );
    } else {
      console.log('Geolocation is not supported by this browser.');
    }
  }

  function onMapClick(event) {
    setCoords({
      longitude: event.latlng.lng,
      latitude: event.latlng.lat
    });
    setSuccess('Coordinates : ' + coords.latitude + ' ' + coords.longitude);
  }

  function isValid() {
    return (
      form.questionText.length > 10 &&
      coords.latitude !== undefined &&
      coords.longitude !== undefined &&
      form.category.categoryName !== undefined
    );
  }

  function onSubmit(e) {
    e.preventDefault();

    const data = {
      questionText: form.questionText,
      category: form.category,
      user: {
        id: user.id
      },
      longitude: coords.longitude,
      latitude: coords.latitude
    };
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      },
      body: JSON.stringify(data)
    };

    fetch('https://immense-harbor-53286.herokuapp.com/questions', options).then(
      response => {
        if (response.status !== 201) {
          setError('fail to answer');
        } else {
          e.preventDefault();
          history.push({
            pathname: '/homepage'
          });
        }
      }
    );
  }

  function needProHelp(e) {
    e.preventDefault();
    history.push({
      pathname: '/professionals'
    });
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <HelpIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Ask a question
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                name="questionText"
                variant="outlined"
                multiline
                required
                fullWidth
                autoFocus
                onChange={onChange}
                value={form.questionText}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2} justify="center">
            <Grid item xs={12}>
              <FormControl required className={classes.formControl}>
                <InputLabel>Category</InputLabel>
                <Select
                  value={form.category}
                  name="category"
                  onChange={onChange}
                  className={classes.selectEmpty}>
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {categories.map(category => (
                    <MenuItem value={category} key={category.id}>
                      <em>{category.categoryName}</em>
                    </MenuItem>
                  ))}
                </Select>
                <FormHelperText>Required</FormHelperText>
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={2} justify="center">
            <Grid item xs={12}>
              <MySwitch
                isOn={value}
                handleToggle={() => setValue(!value)}
                onSwitch={onSwitch}
              />
              <Typography variant="body2" style={{ display: 'inline-block' }}>
                Use browser location?
              </Typography>
            </Grid>
            <div onClick={onClick}>
              <Popup
                trigger={
                  <Button variant="outlined" color="primary">
                    Select location from a map
                  </Button>
                }
                position="right center">
                <div id="mapid">
                  <LeafletMap
                    onClick={onMapClick}
                    center={[coords.latitude, coords.longitude]}
                    zoom={17.5}
                    maxZoom={30}
                    attributionControl
                    zoomControl
                    doubleClickZoom
                    scrollWheelZoom
                    dragging
                    animate
                    easeLinearity={0.35}>
                    <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
                    <Marker position={[coords.latitude, coords.longitude]}>
                      <Popup>
                        This location will be used as the target of your
                        question.
                      </Popup>
                    </Marker>
                  </LeafletMap>
                </div>
              </Popup>
            </div>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}
            onClick={needProHelp}>
            Need professional help?
          </Button>
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}
            disabled={!isValid()}>
            Submit
          </Button>
          <Grid className={classes.error}>{error}</Grid>
          <Grid className={classes.success}>{success}</Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
