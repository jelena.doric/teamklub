import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import QuestionList from 'components/QuestionList';
import UserList from 'components/UserList';
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const UserView = props => {
  const classes = useStyles();
  const { history } = props;

  return (
    <div className={classes.root}>
      {' '}
      <div>
        <Grid>
          <UserList history={history} />
        </Grid>
      </div>
    </div>
  );
};

export default UserView;
