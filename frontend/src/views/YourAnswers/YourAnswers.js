import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import AnsweredQuestionList from 'components/AnsweredQuestionList';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const YourAnswers = props => {
  const classes = useStyles();
  const { history } = props;

  return (
    <div className={classes.root}>
      {' '}
      <div>
        <Grid>
          <AnsweredQuestionList history={history} />
        </Grid>
      </div>
    </div>
  );
};

export default YourAnswers;
