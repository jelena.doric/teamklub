import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import PendingRequestList from "../../components/PendingRequestList";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const YourPendingRequests = props => {
  const classes = useStyles();
  const { history } = props;

  return (
    <div className={classes.root}>
      {' '}
      <div>
        <Grid>
          <PendingRequestList history={history} />
        </Grid>
      </div>
    </div>
  );
};

export default YourPendingRequests;
