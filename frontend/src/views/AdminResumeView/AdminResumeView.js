import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import ResumeList from 'components/ResumeList';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const AdminResumeView = props => {
  const classes = useStyles();
  const { history } = props;

  return (
    <div className={classes.root}>
    {' '}
    <div>
    <Grid>
    <ResumeList history={history} />
  </Grid>
  </div>
  </div>
);
};

export default AdminResumeView;
