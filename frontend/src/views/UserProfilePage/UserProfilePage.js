import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { AccountProfile, AccountDetails, ProDetails } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const UserAccount = props => {
  const classes = useStyles();
  if (props.location.state.user.role.roleName == 'ROLE_PROFESSIONAL') {
    return (
      <div className={classes.root}>
        <Grid container spacing={4}>
          <Grid item lg={4} md={6} xl={4} xs={12}>
            <AccountProfile user={props.location.state.user} />
          </Grid>
          <Grid item lg={8} md={6} xl={8} xs={12}>
            <AccountDetails user={props.location.state.user} />
            <ProDetails
              history={props.history}
              user={props.location.state.user}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
  if (props.location.state.user.role.roleName == 'ROLE_ADMIN') {
    return (
      <div className={classes.root}>
        <Grid container spacing={4}>
          <Grid item lg={4} md={6} xl={4} xs={12}>
            <AccountProfile user={props.location.state.user} />
          </Grid>
          <Grid item lg={8} md={6} xl={8} xs={12}>
            <AccountDetails user={props.location.state.user} />
          </Grid>
        </Grid>
      </div>
    );
  } else {
    return (
      <div className={classes.root}>
        <Grid container spacing={4}>
          <Grid item lg={4} md={6} xl={4} xs={12}>
            <AccountProfile user={props.location.state.user} />
          </Grid>
          <Grid item lg={8} md={6} xl={8} xs={12}>
            <AccountDetails user={props.location.state.user} />
          </Grid>
        </Grid>
      </div>
    );
  }
};

export default UserAccount;
