import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {}
}));

const ProDetails = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const skills = props.user.skills;
  const qualifications = props.user.qualifications;
  const skillsList = [];
  const qualificationsList = [];
  for (let i = 0, l = props.user.skills.length; i < l; i += 1) {
    if (skills[i].skillName) {
      skillsList.push(skills[i].skillName);
    }
  }

  for (let i = 0, l = props.user.qualifications.length; i < l; i += 1) {
    if (qualifications[i].qualificationName) {
      qualificationsList.push(qualifications[i].qualificationName);
    }
  }
  function hower() {}

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <form autoComplete="off" noValidate>
        <CardHeader title="Pro Details" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Typography variant="body3" component="p">
              {qualificationsList.map(qual => (
                <Chip variant="outlined" label={qual} onClick={hower} />
              ))}
              <br />
              <br />
              {skillsList.map(skill => (
                <Chip
                  variant="outlined"
                  color="primary"
                  label={skill}
                  onClick={hower}
                />
              ))}
            </Typography>
          </Grid>
        </CardContent>
        <Divider />
      </form>
    </Card>
  );
};

ProDetails.propTypes = {
  className: PropTypes.string
};

export default ProDetails;
