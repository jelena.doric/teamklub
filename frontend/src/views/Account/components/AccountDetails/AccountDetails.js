import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import AuthenticationService from 'service/AuthenticationService';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
  success: {
    color: 'green'
  }
}));

const AccountDetails = props => {
  const { className, ...rest } = props;
  const user = AuthenticationService.getLoggedInUser();
  const classes = useStyles();

  const [values, setValues] = useState({
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    userName: user.username
  });

  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const [error, setError] = React.useState('');
  const [success, setSuccess] = React.useState('');

  function onSubmit(e) {
    e.preventDefault();

    setError('');
    setSuccess('');
    const id = user.id;
    const data = {
      firstName: values.firstName,
      lastName: values.lastName,
      username: values.userName,
      password: user.password,
      email: values.email,
      userPicturePath: user.userPicturePath,
      id: id
    };
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      },
      body: JSON.stringify(data)
    };
    fetch('https://immense-harbor-53286.herokuapp.com/app-users/' + id, options).then(response => {
      if (response.status !== 200) {
        setError('Username taken');
      } else {
        setSuccess('Details saved successfully, changes will be applied on your next login');
      }
    });
  }

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <form autoComplete="off" noValidate>
        <CardHeader subheader="The information can be edited" title="Profile" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="First name"
                margin="dense"
                name="firstName"
                onChange={handleChange}
                required
                value={values.firstName}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Last name"
                margin="dense"
                name="lastName"
                onChange={handleChange}
                required
                value={values.lastName}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Email Address"
                margin="dense"
                name="email"
                onChange={handleChange}
                required
                value={values.email}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Username"
                margin="dense"
                name="userName"
                onChange={handleChange}
                required
                value={values.userName}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button color="primary" variant="contained" onClick={onSubmit}>
            Save details
          </Button>
          <Grid className={classes.error}>{error}</Grid>
          <Grid className={classes.success}>{success}</Grid>
        </CardActions>
      </form>
    </Card>
  );
};

AccountDetails.propTypes = {
  className: PropTypes.string
};

export default AccountDetails;
