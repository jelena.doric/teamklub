import React from 'react';
import clsx from 'clsx';
import moment from 'moment';
import AuthenticationService from 'service/AuthenticationService';
import { makeStyles } from '@material-ui/styles';
import axios from 'axios';
import PropTypes from 'prop-types';
import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Typography,
  Divider,
  Button,
  LinearProgress
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  details: {
    display: 'flex'
  },
  avatar: {
    marginLeft: 'auto',
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0
  },
  progress: {
    marginTop: theme.spacing(2)
  },
  uploadButton: {
    marginRight: theme.spacing(2)
  }
}));

const AccountProfile = props => {
  const user = AuthenticationService.getLoggedInUser();
  const { className, ...rest } = props;
  const [selectedFile, setSelectedFile] = React.useState('');
  const [fileInput, setFileInput] = React.useState('');
  const [imageSource, setImageSource] = React.useState(
    user.userPicturePath === null
      ? ''
      : 'https://immense-harbor-53286.herokuapp.com/downloadFile/' +
          user.userPicturePath
  );

  const classes = useStyles();

  function fileSelectHandler(event) {
    setSelectedFile(event.target.files[0]);
  }

  function fileUploadHandler(event) {
    const fd = new FormData();
    fd.append('file', selectedFile, selectedFile.name);
    axios
      .post('https://immense-harbor-53286.herokuapp.com/uploadFile', fd)
      .then(res => {
        setImageSource(res.data.fileDownloadUri);
        const data = {
          id: user.id,
          picturePath: res.data.fileName
        };
        const options = {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: AuthenticationService.getToken()
          },
          body: JSON.stringify(data)
        };
        fetch(
          'https://immense-harbor-53286.herokuapp.com/app-users/picture/' +
            user.id,
          options
        ).then(() => console.log());
      });
  }

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <div className={classes.details}>
          <div>
            <Typography gutterBottom variant="h2">
              {user.firstName}
              <br />
              {user.lastName}
            </Typography>
            <Typography
              className={classes.dateText}
              color="textSecondary"
              variant="body1">
              {user.rating === null ? '' : 'Rating : ' + user.rating}
            </Typography>
          </div>
          <Avatar className={classes.avatar} src={imageSource} />
        </div>
      </CardContent>
      <Divider />
      <CardActions>
        <input
          style={{ display: 'none' }}
          type="file"
          onChange={fileSelectHandler}
          ref={fileInput => setFileInput(fileInput)}
        />
        <Button
          onClick={() => fileInput.click()}
          className={classes.uploadButton}
          color="primary"
          variant="outlined">
          Choose picture
        </Button>
        <Button
          onClick={fileUploadHandler}
          className={classes.uploadButton}
          color="primary"
          variant="outlined">
          Upload picture
        </Button>
      </CardActions>
    </Card>
  );
};

AccountProfile.propTypes = {
  className: PropTypes.string
};

export default AccountProfile;
