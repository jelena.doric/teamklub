import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import AuthenticationService from 'service/AuthenticationService';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {}
}));

const ProDetails = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const user = AuthenticationService.getLoggedInUser();
  const skills = user.skills;
  const qualifications = user.qualifications;
  const skillsList = [];
  const qualificationsList = [];
  for (let i = 0, l = user.skills.length; i < l; i += 1) {
    if (skills[i].skillName) {
      skillsList.push(skills[i].skillName);
    }
  }

  for (let i = 0, l = user.qualifications.length; i < l; i += 1) {
    if (qualifications[i].qualificationName) {
      qualificationsList.push(qualifications[i].qualificationName);
    }
  }
  function becomePro(e) {
    e.preventDefault();
    props.history.push('/profesional');
  }

  function hover() {}

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <form autoComplete="off" noValidate>
        <CardHeader subheader="The information can be edited" title="Pro" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Typography variant="body3" component="p">
              {qualificationsList.map(qual => (
                <Chip variant="outlined" label={qual} onClick={hover} />
              ))}
              <br />
              <br />
              {skillsList.map(skill => (
                <Chip
                  variant="outlined"
                  color="primary"
                  label={skill}
                  onClick={hover}
                />
              ))}
            </Typography>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button color="primary" variant="contained" onClick={becomePro}>
            Update skills and qualifications
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

ProDetails.propTypes = {
  className: PropTypes.string
};

export default ProDetails;
