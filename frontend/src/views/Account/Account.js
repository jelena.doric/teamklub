import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import AuthenticationService from 'service/AuthenticationService';
import { Button } from '@material-ui/core';

import { AccountProfile, AccountDetails, ProDetails } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Account = props => {
  const classes = useStyles();
  const user = AuthenticationService.getLoggedInUser();
  function becomePro(e) {
    e.preventDefault();
    props.history.push('/profesional');
  }
  if (user.role.roleName === 'ROLE_PROFESSIONAL') {
    return (
      <div className={classes.root}>
        <Grid container spacing={4}>
          <Grid item lg={4} md={6} xl={4} xs={12}>
            <AccountProfile />
          </Grid>
          <Grid item lg={8} md={6} xl={8} xs={12}>
            <AccountDetails />
            <ProDetails history={props.history} />
          </Grid>
        </Grid>
      </div>
    );
  }
  if (user.role.roleName === 'ROLE_ADMIN') {
    return (
      <div className={classes.root}>
        <Grid container spacing={4}>
          <Grid item lg={4} md={6} xl={4} xs={12}>
            <AccountProfile />
          </Grid>
          <Grid item lg={8} md={6} xl={8} xs={12}>
            <AccountDetails />
          </Grid>
        </Grid>
      </div>
    );
  } else {
    return (
      <div className={classes.root}>
        <Grid container spacing={4}>
          <Grid item lg={4} md={6} xl={4} xs={12}>
            <AccountProfile />
          </Grid>
          <Grid item lg={8} md={6} xl={8} xs={12}>
            <AccountDetails />
          </Grid>
          <Button color="primary" variant="contained" onClick={becomePro}>
            Become pro
          </Button>
        </Grid>
      </div>
    );
  }
};

export default Account;
