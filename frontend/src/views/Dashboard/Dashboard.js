import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import QuestionList from 'components/QuestionList';
import HomeQuestionList from 'components/HomeQuestionList';
import AuthenticationService from 'service/AuthenticationService';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Dashboard = props => {
  console.log(props);
  const classes = useStyles();
  const { history } = props;
  const user = AuthenticationService.getLoggedInUser();
  if (user.role.roleName === 'ROLE_ADMIN') {
    return (
      <div className={classes.root}>
        {' '}
        <div>
          <Grid>
            <QuestionList history={history} user={user} />
          </Grid>
        </div>
      </div>
    );
  } else {
    return (
      <div className={classes.root}>
        {' '}
        <div>
          <Grid>
            <HomeQuestionList history={history} user={user} />
          </Grid>
        </div>
      </div>
    );
  }
};

export default Dashboard;
