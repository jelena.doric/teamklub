import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import AuthenticationService from 'service/AuthenticationService';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'TeamKlub '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  error: {
    color: 'red'
  },
  success: {
    color: 'green'
  }
}));

export default function GoAnswer(props) {
  const classes = useStyles();
  const [form, setForm] = React.useState({
    yourAdminQuestion: ''
  });
  const [error, setError] = React.useState('');
  const [success, setSuccess] = React.useState('');
  const { history } = props;
  function onChange(event) {
    const { name, value } = event.target;
    setForm(oldForm => ({ ...oldForm, [name]: value }));
  }

  function onSubmit(e) {
    e.preventDefault();
    setError('');
    setSuccess('');
    const data = {
      messageText: form.yourAdminQuestion,
      sender: {
        id: 3
      }
    };
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      },
      body: JSON.stringify(data)
    };
    fetch('https://immense-harbor-53286.herokuapp.com/messages', options).then(response => {
      if (response.status % 4 === 0 || response.status % 5 === 0) {
        setError("question didn't reach the admin");
      } else {
        e.preventDefault();
        history.push({
          pathname: '/homepage'
        });
      }
    });
  }

  function isValid() {
    const { yourAdminQuestion } = form;
    return yourAdminQuestion.length > 10;
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <QuestionAnswerIcon /> //TODO culjo treba srediti
        </Avatar>
        <Typography component="h1" variant="h5">
          Ask Admin
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                name="yourAdminQuestion"
                variant="outlined"
                required
                fullWidth
                id="yourAdminQuestion"
                label="Your Question"
                autoFocus
                onChange={onChange}
                value={form.yourAdminQuestion}
              />
            </Grid>
            <Grid className={classes.error}>{error}</Grid>
            <Grid className={classes.success}>{success}</Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}
            disabled={!isValid()}>
            Submit
          </Button>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
