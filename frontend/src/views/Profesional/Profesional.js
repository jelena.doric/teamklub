import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import AuthenticationService from 'service/AuthenticationService';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'TeamKlub '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  error: {
    color: 'red'
  },
  success: {
    color: 'green'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  root: {
    margin: 'auto'
  },
  button: {
    margin: theme.spacing(0.5, 0)
  }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium
  };
}

export default function Profesional(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [form, setForm] = React.useState({
    category: {},
    qualification: {},
    skill: {}
  });
  const [categories, setCategories] = React.useState([]);
  const [selectedCategories, setSelectedCategories] = React.useState([]);
  const [qualifications, setQualifications] = React.useState([]);
  const [selectedQualifications, setSelectedQualifications] = React.useState(
    []
  );
  const [skills, setSkills] = React.useState([]);
  const [selectedSkills, setSelectedSkills] = React.useState([]);
  const [error, setError] = React.useState('');
  const [success, setSuccess] = React.useState('');

  const categoriesList = [];
  for (let i = 0, l = categories.length; i < l; i += 1) {
    if (categories[i].categoryName) {
      categoriesList.push(categories[i]);
    }
  }
  const qualificationsList = [];
  for (let i = 0, l = qualifications.length; i < l; i += 1) {
    if (qualifications[i].qualificationName) {
      qualificationsList.push(qualifications[i]);
    }
  }
  const skillsList = [];
  for (let i = 0, l = skills.length; i < l; i += 1) {
    if (skills[i].skillName) {
      skillsList.push(skills[i]);
    }
  }
  const handleChangeQualifications = event => {
    setSelectedQualifications(event.target.value);
  };

  const handleChangeSkills = event => {
    setSelectedSkills(event.target.value);
  };

  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: AuthenticationService.getToken()
    }
  };

  React.useEffect(() => {
    fetch('https://immense-harbor-53286.herokuapp.com/categories', options)
      .then(data => data.json())
      .then(categories => setCategories(categories));
  }, []);
  React.useEffect(() => {
    fetch('https://immense-harbor-53286.herokuapp.com/qualifications', options)
      .then(data => data.json())
      .then(qualifications => setQualifications(qualifications));
  }, []);
  React.useEffect(() => {
    fetch('https://immense-harbor-53286.herokuapp.com/skills', options)
      .then(data => data.json())
      .then(skills => setSkills(skills));
  }, []);

  function onSubmit(e) {
    e.preventDefault();
    setError('');
    setSuccess('');
    const data = {
      user: {
        id: 3
      },
      category: form.category,
      qualifications: selectedQualifications,
      skills: selectedSkills
    };

    console.log(data);

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      },
      body: JSON.stringify(data)
    };
    fetch('https://immense-harbor-53286.herokuapp.com/resumes', options).then(response => {
      if (response.status % 4 === 0 || response.status % 5 === 0) {
        setError('error');
      } else {
        setSuccess('Request sucessfully sent');
      }
    });
  }

  function isValid() {
    return (
      form.category.categoryName !== undefined &&
      selectedQualifications.length > 0 &&
      selectedSkills.length > 0
    );
  }
  function onChange(event) {
    const { name, value } = event.target;
    setForm(oldForm => ({ ...oldForm, [name]: value }));
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LocalLibraryIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Become a professional
        </Typography>
        <FormControl required className={classes.formControl}>
          <InputLabel>Category</InputLabel>
          <Select
            value={form.category}
            name="category"
            onChange={onChange}
            className={classes.selectEmpty}>
            {categories.map(category => (
              <MenuItem value={category} key={category.id}>
                <em>{category.categoryName}</em>
              </MenuItem>
            ))}
          </Select>
          <FormHelperText>Required</FormHelperText>
        </FormControl>
        <FormControl required className={classes.formControl}>
          <InputLabel>Qualifications</InputLabel>
          <Select
            multiple
            value={selectedQualifications}
            input={<Input />}
            onChange={handleChangeQualifications}
            className={classes.selectEmpty}>
            {qualificationsList.map(qualification => (
              <MenuItem
                key={qualification.id}
                value={qualification}
                style={getStyles(
                  qualification.qualificationName,
                  selectedQualifications,
                  theme
                )}>
                {qualification.qualificationName}
              </MenuItem>
            ))}
          </Select>
          <FormHelperText>Required</FormHelperText>
        </FormControl>
        <FormControl required className={classes.formControl}>
          <InputLabel>Skills</InputLabel>
          <Select
            multiple
            value={selectedSkills}
            input={<Input />}
            onChange={handleChangeSkills}
            className={classes.selectEmpty}>
            {skillsList.map(skill => (
              <MenuItem
                key={skill.id}
                value={skill}
                style={getStyles(skill.skillName, selectedSkills, theme)}>
                {skill.skillName}
              </MenuItem>
            ))}
          </Select>
          <FormHelperText>Required</FormHelperText>
        </FormControl>
        <Grid className={classes.error}>{error}</Grid>
        <Grid className={classes.success}>{success}</Grid>
        <Button
          type="submit"
          fullWidth
          variant="outlined"
          color="primary"
          onClick={onSubmit}
          className={classes.submit}
          disabled={!isValid()}>
          Submit
        </Button>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
