import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import Rating from '@material-ui/lab/Rating';
import axios from 'axios';
import AuthenticationService from '../../service/AuthenticationService';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'TeamKlub '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  error: {
    color: 'red'
  },
  success: {
    color: 'green'
  },
  rating: {
    width: 200,
    alignItems: 'center'
  }
}));

export default function RateProfessional(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(2);
  const [error, setError] = React.useState('');
  const { history } = props;

  function onSubmit(e) {
    e.preventDefault();
    setError('');
    const data = {
      rating: value,
      id: 3
    };
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthenticationService.getToken()
      },
      body: JSON.stringify(data)
    };
    console.log(AuthenticationService.isUserLoggedIn());
    fetch(
      'https://immense-harbor-53286.herokuapp.com/app-users/professional/rating/' + data.id,
      options
    ).then(response => {
      if (response.status !== 200) {
        setError('fail to answer');
      } else {
        e.preventDefault();
        history.push({
          pathname: '/yourrequests',
          id: props.location.id
        });
      }
    });
    // axios
    //   .put('https://immense-harbor-53286.herokuapp.com/app-users/professional/rating/' + 3, {
    //     rating: value,
    //     id: 3
    //   })
    //   .then(response => {
    //     if (response.status % 4 === 0 || response.status % 5 === 0) {
    //       setError('fail to answer');
    //     } else {
    //       e.preventDefault();
    //       history.push({
    //         pathname: '/answers',
    //         id: props.location.id
    //       });
    //     }
    //   });
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <QuestionAnswerIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          How would you rate your professional?
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <Grid container spacing={2} direction="column" align="center">
            <Grid item xs={12}>
              <Box
                component="fieldset"
                mb={3}
                borderColor="transparent"
                className={classes.rating}>
                <Rating
                  name="simple-controlled"
                  value={value}
                  onChange={(event, newValue) => {
                    setValue(newValue);
                  }}
                />
              </Box>
            </Grid>
            <Grid className={classes.error}>{error}</Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}>
            Submit
          </Button>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
