import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, IconButton, Grid } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: 'none',
    color: 'White'
  },
  flexGrow: {
    flexGrow: 1
  },
  signOutButton: {
    marginLeft: theme.spacing(1)
  }
}));

const Topbar = props => {
  const { className, onSidebarOpen, ...rest } = props;
  const { history } = props;
  const classes = useStyles();

  const handleBack = () => {
    console.log(history);
    history.goBack();
  };

  return (
    <AppBar {...rest} className={clsx(classes.root, className)}>
      <Toolbar>
        <Grid>TeamKlub</Grid>
        <div className={classes.flexGrow} />
        <div className={classes.contentHeader}>
          <IconButton onClick={onSidebarOpen} className={classes.root}>
            <ArrowDownwardIcon />
          </IconButton>
          <IconButton onClick={handleBack} className={classes.root}>
            <ArrowBackIcon />
          </IconButton>
        </div>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

export default Topbar;
