import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Divider, Drawer } from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Dashboard';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Profile, SidebarNav } from './components';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import HelpIcon from '@material-ui/icons/Help';
import AuthenticationService from 'service/AuthenticationService';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import CategoryIcon from '@material-ui/icons/Category';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, ...rest } = props;
  const user = AuthenticationService.getLoggedInUser();
  const classes = useStyles();
  const pages = [
    {
      title: 'Home Page',
      href: '/homepage',
      icon: <DashboardIcon />
    },
    {
      title: 'Profile Page',
      href: '/profilepage',
      icon: <AccountBoxIcon />
    },
    {
      title: 'Ask a Question',
      href: '/askquestion',
      icon: <HelpIcon />
    },
    {
      title: 'Your Questions',
      href: '/yourquestions',
      icon: <LiveHelpIcon />
    },
    {
      title: 'Your Answers',
      href: '/youranswers',
      icon: <SupervisedUserCircleIcon />
    },
    {
      title: 'Your Requests',
      href: '/yourrequests',
      icon: <LiveHelpIcon />
    },
    {
      title: 'Logout',
      href: '/signin',
      icon: <ArrowBackIcon />
    }
  ];
  const professionalPages = [
    {
      title: 'Home Page',
      href: '/homepage',
      icon: <DashboardIcon />
    },
    {
      title: 'Profile Page',
      href: '/profilepage',
      icon: <AccountBoxIcon />
    },
    {
      title: 'Ask a Question',
      href: '/askquestion',
      icon: <HelpIcon />
    },
    {
      title: 'Your Questions',
      href: '/yourquestions',
      icon: <LiveHelpIcon />
    },
    {
      title: 'Your Answers',
      href: '/youranswers',
      icon: <SupervisedUserCircleIcon />
    },
    {
      title: 'Your Requests',
      href: '/yourrequests',
      icon: <LiveHelpIcon />
    },
    {
      title: 'Your Pending Requests',
      href: '/yourpendingrequests',
      icon: <ContactMailIcon />
    },
    {
      title: 'Logout',
      href: '/signin',
      icon: <ArrowBackIcon />
    }
  ];
  const adminPages = [
    {
      title: 'Home Page',
      href: '/homepage',
      icon: <DashboardIcon />
    },
    {
      title: 'Profile Page',
      href: '/profilepage',
      icon: <AccountBoxIcon />
    },
    {
      title: 'Ask a Question',
      href: '/askquestion',
      icon: <HelpIcon />
    },
    {
      title: 'Your Questions',
      href: '/yourquestions',
      icon: <LiveHelpIcon />
    },
    {
      title: 'Your Answers',
      href: '/youranswers',
      icon: <SupervisedUserCircleIcon />
    },
    {
      title: 'Your Requests',
      href: '/yourrequests',
      icon: <LiveHelpIcon />
    },
    {
      title: 'Create Category',
      href: '/create-category',
      icon: <CategoryIcon />
    },
    {
      title: 'User List',
      href: '/userlist',
      icon: <AssignmentIndIcon />
    },
    {
      title: 'Admin Questions',
      href: '/admin-questions',
      icon: <LiveHelpIcon />
    },
    {
      title: 'Resumes',
      href: '/admin-resume-view',
      icon: <VerifiedUserIcon />
    },
    {
      title: 'Logout',
      href: '/signin',
      icon: <ArrowBackIcon />
    }
  ];
  if (user.role.roleName === 'ROLE_PROFESSIONAL') {
    return (
      <Drawer
        anchor="left"
        classes={{ paper: classes.drawer }}
        onClose={onClose}
        open={open}
        variant={variant}>
        <div {...rest} className={clsx(classes.root, className)}>
          <Profile />
          <Divider className={classes.divider} />
          <SidebarNav
            className={classes.nav}
            pages={professionalPages}
            closeSidebar={props.closeSidebar}
          />
        </div>
      </Drawer>
    );
  }
  if (user.role.roleName === 'ROLE_ADMIN') {
    return (
      <Drawer
        anchor="left"
        classes={{ paper: classes.drawer }}
        onClose={onClose}
        open={open}
        variant={variant}>
        <div {...rest} className={clsx(classes.root, className)}>
          <Profile />
          <Divider className={classes.divider} />
          <SidebarNav
            className={classes.nav}
            pages={adminPages}
            closeSidebar={props.closeSidebar}
          />
        </div>
      </Drawer>
    );
  } else {
    return (
      <Drawer
        anchor="left"
        classes={{ paper: classes.drawer }}
        onClose={onClose}
        open={open}
        variant={variant}>
        <div {...rest} className={clsx(classes.root, className)}>
          <Profile />
          <Divider className={classes.divider} />
          <SidebarNav
            className={classes.nav}
            pages={pages}
            closeSidebar={props.closeSidebar}
          />
        </div>
      </Drawer>
    );
  }
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;
