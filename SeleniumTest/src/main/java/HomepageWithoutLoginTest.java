import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
public class HomepageWithoutLoginTest {
	
		public static void main(String[] args) {
			System.setProperty("webdriver.gecko.driver", "/usr/bin//geckodriver");
			
			WebDriver driver = new FirefoxDriver();
			
			String baseUrl = "http://frontend-opp.herokuapp.com/homepage";
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

			driver.get(baseUrl);
			
			String url = driver.getCurrentUrl();
			if (url.equals("http://frontend-opp.herokuapp.com/signin")) System.out.println("Test passed");
			else System.out.println("Test failed");
			
			driver.close();
			System.exit(0);
		}

}
