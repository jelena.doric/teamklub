import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AskAQuestionTest {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
		WebDriver driver = new FirefoxDriver();
		WebElement element;
		String baseUrl = "http://frontend-opp.herokuapp.com/";
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		driver.get(baseUrl);
		
		element = driver.findElement(By.name("username"));
		element.sendKeys("SeleniumTests");
		element = driver.findElement(By.name("password"));
		element.sendKeys("selenium");
		
		driver.findElement(By.cssSelector(".MuiButton-label")).click();

		driver.findElement(By.cssSelector("li.MuiListItem-root:nth-child(3)")).click();
		
		element = driver.findElement(By.cssSelector("textarea.MuiInputBase-input:nth-child(1)"));
		element.sendKeys("Does this Selenium test work?");
		
		element = driver.findElement(By.cssSelector("#mui-component-select-category"));
		driver.findElement(By.cssSelector("#mui-component-select-category")).click();
		driver.findElement(By.cssSelector("li.MuiButtonBase-root:nth-child(2)")).click();
		
		driver.findElement(By.cssSelector("button.MuiButtonBase-root:nth-child(6) > span:nth-child(1)")).click();
	
		
		
		driver.findElement(By.cssSelector("li.MuiListItem-root:nth-child(4) > div:nth-child(1) > a:nth-child(1)")).click();
		
		element = driver.findElement(By.cssSelector("#searchInput"));
		element.sendKeys("Does this Selenium test work?");
		
		
		boolean isItThere = driver.findElements(By.cssSelector("div.MuiGrid-item:nth-child(1) > div:nth-child(1)")).size() != 0;
		
		if(isItThere) {
			System.out.println("Test passed");
		} else {
			System.out.println("Test failed");
		}
		
		driver.close();
	}

}
