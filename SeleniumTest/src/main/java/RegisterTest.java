import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RegisterTest {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
		WebDriver driver = new FirefoxDriver();
		WebElement element;
		String baseUrl = "http://frontend-opp.herokuapp.com/";
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		
		driver.get(baseUrl);
		
		driver.findElement(By.cssSelector("a.MuiTypography-root")).click();
		
		element = driver.findElement(By.name("firstName"));
		element.sendKeys("Selenium");
		element = driver.findElement(By.name("lastName"));
		element.sendKeys("Test");
		element = driver.findElement(By.name("userName"));
		element.sendKeys("SeleniumTests");
		element = driver.findElement(By.name("email"));
		element.sendKeys("selenium@gmail.com");
		element = driver.findElement(By.name("userPassword"));
		element.sendKeys("password");
		
		
		driver.findElement(By.cssSelector(".MuiButtonBase-root")).click();
		
		
		element = driver.findElement(By.cssSelector("div.MuiGrid-root:nth-child(6)"));
		String html = element.getAttribute("innerHTML");
		
		String check = "username or email already taken";
		
		if(html.equals(check)) {
			System.out.println("Test passed.");
		}
		
		driver.close();
		
		
	}
	
	
}
