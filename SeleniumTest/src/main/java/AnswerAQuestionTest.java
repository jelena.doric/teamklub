import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AnswerAQuestionTest {
	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
		WebDriver driver = new FirefoxDriver();
		WebElement element;
		String baseUrl = "http://frontend-opp.herokuapp.com/";
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		driver.get(baseUrl);
		
		element = driver.findElement(By.name("username"));
		element.sendKeys("SeleniumTests");
		element = driver.findElement(By.name("password"));
		element.sendKeys("selenium");
		
		driver.findElement(By.cssSelector(".MuiButton-label")).click();

		element = driver.findElement(By.cssSelector("#searchInput"));
		element.sendKeys("Does this selenium test work");

		driver.findElement(By.cssSelector("div.MuiGrid-item:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(1) > span:nth-child(1)")).click();
		
		element = driver.findElement(By.cssSelector("#yourAnswer"));
		element.sendKeys("It works because...");
		
		driver.findElement(By.cssSelector("button.MuiButton-root > span:nth-child(1)")).click();
		
		element = driver.findElement(By.cssSelector("#searchInput"));
		element.sendKeys("It works because...");
		
		boolean isItThere = driver.findElements(By.cssSelector(".MuiCard-root")).size() != 0;
		
		if(isItThere) {
			System.out.println("Answer was recorded. Test passed");
		} else {
			System.out.println("Test failed!");
		}
		
		driver.close();
	}
}
