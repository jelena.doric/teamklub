package opp.backend;


import opp.domain.AppUser;
import opp.domain.Role;
import opp.domain.Skill;
import org.apache.catalina.Lifecycle;
import org.hibernate.validator.constraints.URL;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BackendApplicationTests {

    @LocalServerPort
    private int serverPort;

    private String BASE_URI;

    private long EXISTING_SKILL_ID = 101;

    private RestTemplate rest;

    @Before
    public void setup() {
        BASE_URI = "http://localhost:" + serverPort;
        rest = new RestTemplate();
    }

    @Test
    public void should_create_role() {
        Role newRole = new Role();
        newRole.setRoleName("testRole3");
        ResponseEntity<Role> createEmployeeResponse = rest.postForEntity(BASE_URI + "/roles", newRole, Role.class);
        Role createdRole = createEmployeeResponse.getBody();
        long roleId = createdRole.getId();
        System.out.println("createEmployeeResponseBody = " + roleId);
        assert (createEmployeeResponse.getStatusCodeValue() == 201);
        rest.delete(BASE_URI + "/roles/" + roleId);
    }


    @Test
    public void should_login_authorized() {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("username", "username");
        body.add("password", "password");
        HttpEntity<MultiValueMap<String, String>> loginRequest = new HttpEntity<>(body, header);
        ResponseEntity<String> loginResponse = rest.postForEntity(BASE_URI + "/login", loginRequest, String.class);
        assert (loginResponse.getStatusCodeValue() == 200);
    }

    @Test
    public void should_list_roles_if_any_exist() {
        ResponseEntity<Role[]> roleResponseEntity = rest.getForEntity(BASE_URI + "/roles", Role[].class);
        assert (roleResponseEntity.getBody().length > 0);
    }

    @Test
    public void should_return_existing_skill() {
        ResponseEntity<Skill> skillResponseEntity = rest.getForEntity(BASE_URI + "/skills/" + EXISTING_SKILL_ID, Skill.class);
        assert (!skillResponseEntity.getBody().getSkillName().equals(null));
    }

    @Test
    public void should_delete_skill() {
        Skill skillToBeCreatedThenDelete = new Skill();
        skillToBeCreatedThenDelete.setSkillName("skillNameTest");
        ResponseEntity<Skill> postResponseEntity = rest.postForEntity(BASE_URI + "/skills", skillToBeCreatedThenDelete, Skill.class);
        System.out.println("skillToBeCreatedThenDelete = " + skillToBeCreatedThenDelete.getId());
        rest.delete(BASE_URI + "/skills/" + postResponseEntity.getBody().getId());
        try {
            rest.getForEntity(BASE_URI + "/skills/" + postResponseEntity.getBody().getId(), Skill.class);
        } catch (HttpClientErrorException e) {
            System.out.println(e.getRawStatusCode());
            assert (e.getRawStatusCode() == 400);
        }
    }

    @Test
    public void should_not_login_unauthorized() {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("username", "newTestUser");
        body.add("password", "testUserPassword");
        HttpEntity<MultiValueMap<String, String>> loginRequest = new HttpEntity<>(body, header);
        try {
            rest.postForEntity(BASE_URI + "/login", loginRequest, String.class);
        } catch (HttpClientErrorException e) {
            assert (e.getRawStatusCode() == 401);
        }
    }
}
