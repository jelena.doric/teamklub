package opp.web.dto;

import opp.domain.AppUser;
import opp.domain.Category;

public class QuestionDTO {

	private AppUser user;

	private String questionText;

	private Category category;

	private Double longitude;

	private Double latitude;

	private String questionPicturePath;

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getQuestionPicturePath() {
		return questionPicturePath;
	}

	public void setQuestionPicturePath(String questionPicturePath) {
		this.questionPicturePath = questionPicturePath;
	}
}
