package opp.web.dto;

public class CurrentUserDTO {

    String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
