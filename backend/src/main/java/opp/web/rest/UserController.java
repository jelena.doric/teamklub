package opp.web.rest;

import opp.domain.AppUser;
import opp.service.AppUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

	private final AppUserService appUserService;

	@Value("${opp.admin.password}")
	private String adminPasswordHash;

	@Value("${opp.admin.email}")
	private String adminEmail;

	public UserController(AppUserService appUserService) {
		this.appUserService = appUserService;
	}

	@GetMapping
	public AppUser getCurrentUser(@AuthenticationPrincipal User user) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();

		return username.equals("admin") ? new AppUser("admin", adminPasswordHash, adminEmail) :
				appUserService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
	}
}
