package opp.web.rest;

import opp.domain.AppUser;
import opp.domain.Question;
import opp.domain.Request;
import opp.domain.Resume;
import opp.web.dto.AppUserDTO;
import opp.web.dto.PicturePathDTO;
import opp.service.AppUserService;
import opp.web.dto.UserRatingDTO;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/app-users")
public class AppUserController {

	private final AppUserService appUserService;
	private final ModelMapper modelMapper;

	public AppUserController(AppUserService userService, ModelMapper modelMapper) {
		this.appUserService = userService;
		this.modelMapper = modelMapper;
	}

	@GetMapping
	public List<AppUser> listUsers() {
		return appUserService.listAll();
	}

	@GetMapping("/professionals")
	public List<AppUser> listProfessionalUsers() {
		return appUserService.listProfessionalUsers();
	}

	@GetMapping("/{id}")
	public AppUser getUser(@PathVariable("id") long id) {
		return appUserService.fetch(id);
	}

	@GetMapping("/questions/asked/{id}")
	public List<Question> listUserAskedQuestions(@PathVariable("id") long id) {
		return appUserService.listAskedQuestions(id);
	}

	@GetMapping("/questions/answered/{id}")
	public List<Question> listUserAnsweredQuestions(@PathVariable("id") long id) {
		return appUserService.listAnsweredQuestions(id);
	}

	@GetMapping("/requests/asked/{id}")
	public List<Request> listUserRequests(@PathVariable("id") long id) {
		return appUserService.listUserRequests(id);
	}

	@GetMapping("/requests/answered/{id}")
	public List<Request> listReceivedRequests(@PathVariable("id") long id) {
		return appUserService.listReceivedRequests(id);
	}

	@PostMapping
	public ResponseEntity<AppUser> createUser(@RequestBody AppUserDTO userDTO) {
		AppUser user = modelMapper.map(userDTO, AppUser.class);

		AppUser saved = appUserService.createUser(user);
		return ResponseEntity.created(URI.create(String.format("/app-users/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/professional/{id}")
	public ResponseEntity<AppUser> createOrUpdateProfessionalUser(@PathVariable("id") Long id, @RequestBody Resume resume) {
		AppUser user = appUserService.fetch(id);

		if (!user.getId().equals(id))
			throw new IllegalArgumentException("User ID must be preserved");

		AppUser saved = appUserService.createOrUpdateProfessionalUser(user, resume.getCategory(), resume.getQualifications(), resume.getSkills());
		return ResponseEntity.created(URI.create(String.format("/app-users/professional/%d", saved.getId()))).body(saved);
	}

    @PutMapping("/{id}")
    public AppUser updateUser(@PathVariable("id") Long id, @RequestBody AppUserDTO userDTO) {
        AppUser user = modelMapper.map(userDTO, AppUser.class);

		if (!user.getId().equals(id))
			throw new IllegalArgumentException("User ID must be preserved");

        AppUser oldUser = appUserService.fetch(id);
        
        user.setRole(oldUser.getRole());
        user.setQualifications(oldUser.getQualifications());
        user.setSkills(oldUser.getSkills());
        user.setRating(oldUser.getRating());
        user.setNumberOfEngagements(oldUser.getNumberOfEngagements());
        user.setCategory(oldUser.getCategory());

        return appUserService.updateUser(user);
    }

    @PutMapping("/picture/{id}")
	public AppUser updateProfilePicture(@PathVariable("id") Long id, @RequestBody PicturePathDTO picturePathDTO) {
		if (!picturePathDTO.getId().equals(id))
			throw new IllegalArgumentException("User ID must be preserved");

		return appUserService.updateProfilePicture(id, picturePathDTO.getPicturePath());
	}

	@PutMapping("/admin/rights/{id}")
	public AppUser grantAdminRights(@PathVariable("id") Long id) {
		return appUserService.grantAdminRights(id);
	}

	@PutMapping("/professional/rating/{id}")
	public AppUser giveRating(@PathVariable("id") Long id, @RequestBody UserRatingDTO ratingDTO) {
		AppUser user = appUserService.fetch(id);

		if (!ratingDTO.getId().equals(user.getId()))
			throw new IllegalArgumentException("User ID must be preserved");

		return appUserService.giveRating(id, ratingDTO.getRating());
	}

	@PutMapping("/ban/{id}")
	public AppUser banUser(@PathVariable("id") Long id) {
		return appUserService.banUser(id);
	}

	@PutMapping("/deactivate/{id}")
	public AppUser deactivateUser(@PathVariable("id") Long id) {
		return appUserService.deactivateUser(id);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public AppUser deleteUser(@PathVariable("id") Long id) {
		return appUserService.deleteUser(id);
	}
}
