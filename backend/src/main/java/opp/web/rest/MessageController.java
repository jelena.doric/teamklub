package opp.web.rest;

import opp.domain.AdminMessage;
import opp.service.AdmimMessageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {

	private final AdmimMessageService admimMessageService;

	public MessageController(AdmimMessageService admimMessageService) {
		this.admimMessageService = admimMessageService;
	}

	@GetMapping
	public List<AdminMessage> listMessages() {
		return admimMessageService.listAll();
	}

	@GetMapping("/{id}")
	public AdminMessage getMessage(@PathVariable("id") long id) {
		return admimMessageService.fetch(id);
	}

	@PostMapping
	public ResponseEntity<AdminMessage> createMessage(@RequestBody AdminMessage message) {
		AdminMessage saved = admimMessageService.createMessage(message);
		return ResponseEntity.created(URI.create(String.format("/messages/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public AdminMessage updateMessage(@PathVariable("id") Long id, @RequestBody AdminMessage message) {
		if (!message.getId().equals(id))
			throw new IllegalArgumentException("Message ID must be preserved");
		return admimMessageService.updateMessage(message);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public AdminMessage deleteMessage(@PathVariable("id") long messageId) {
		return admimMessageService.deleteMessage(messageId);
	}
}