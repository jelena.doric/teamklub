package opp.web.rest;

import opp.domain.AnswerToRequest;
import opp.domain.Request;
import opp.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/requests")
public class RequestController {

	private final RequestService requestService;

	public RequestController(RequestService requestService) {
		this.requestService = requestService;
	}

	@GetMapping
	public List<Request> listRequests() {
		return requestService.listAll();
	}

	@GetMapping("/{id}/answers")
	public List<AnswerToRequest> listAnswers(@PathVariable("id") long id) {
		return requestService.listAnswers(id);
	}


	@GetMapping("/{id}")
	public Request getRequest(@PathVariable("id") long id) {
		return requestService.fetch(id);
	}

	@PostMapping
	public ResponseEntity<Request> createRequest(@RequestBody Request request) {
		Request saved = requestService.createRequest(request);
		return ResponseEntity.created(URI.create(String.format("/requests/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
	public Request updateRequest(@PathVariable("id") Long id, @RequestBody Request request) {
		if (!request.getId().equals(id))
			throw new IllegalArgumentException("Request ID must be preserved");

		return requestService.updateRequest(request);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Request deleteRequest(@PathVariable("id") long requestId) {
		return requestService.deleteRequest(requestId);
	}
}
