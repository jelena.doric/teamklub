package opp.web.rest;

import opp.domain.Skill;
import opp.service.SkillService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/skills")
public class SkillController {

	private final SkillService skillService;

	public SkillController(SkillService skillService) {
		this.skillService = skillService;
	}

	@GetMapping
	public List<Skill> listSkills() {
		return skillService.listAll();
	}

	@GetMapping("/{id}")
	public Skill getSkill(@PathVariable("id") long id) {
		return skillService.fetch(id);
	}

	@PostMapping
//    @Secured("ROLE_ADMIN")
	public ResponseEntity<Skill> createSkill(@RequestBody Skill skill) {
		Skill saved = skillService.createSkill(skill);
		return ResponseEntity.created(URI.create(String.format("/skills/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Skill updateSkill(@PathVariable("id") Long id, @RequestBody Skill skill) {
		if (!skill.getId().equals(id))
			throw new IllegalArgumentException("Skill ID must be preserved");
		return skillService.updateSkill(skill);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Skill deleteSkill(@PathVariable("id") long skillId) {
		return skillService.deleteSkill(skillId);
	}
}
