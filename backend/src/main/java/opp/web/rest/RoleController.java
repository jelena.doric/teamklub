package opp.web.rest;

import opp.domain.Role;
import opp.service.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/roles")
public class RoleController {

	private final RoleService roleService;

	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}

	@GetMapping
	public List<Role> listRoles() {
		return roleService.listAll();
	}

	@GetMapping("/{id}")
	public Role getRole(@PathVariable("id") long id) {
		return roleService.fetch(id);
	}

	@PostMapping
	public ResponseEntity<Role> createRole(@RequestBody Role role) {
		Role saved = roleService.createRole(role);
		return ResponseEntity.created(URI.create(String.format("/roles/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Role updateRole(@PathVariable("id") Long id, @RequestBody Role role) {
		if (!role.getId().equals(id))
			throw new IllegalArgumentException("Role ID must be preserved");
		return roleService.updateRole(role);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Role deleteRole(@PathVariable("id") long roleId) {
		return roleService.deleteRole(roleId);
	}
}