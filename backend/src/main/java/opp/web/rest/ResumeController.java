package opp.web.rest;

import opp.domain.Resume;
import opp.service.ResumeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/resumes")
public class ResumeController {

	private final ResumeService resumeService;

	public ResumeController(ResumeService resumeService) {
		this.resumeService = resumeService;
	}

	@GetMapping
	public List<Resume> listResumes() {
		return resumeService.listAll();
	}

	@GetMapping("/{id}")
	public Resume getResume(@PathVariable("id") long id) {
		return resumeService.fetch(id);
	}

	@PostMapping
	public ResponseEntity<Resume> createResume(@RequestBody Resume resume) {
		Resume saved = resumeService.createResume(resume);
		return ResponseEntity.created(URI.create(String.format("/resumes/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Resume updateResume(@PathVariable("id") Long id, @RequestBody Resume resume) {
		if (!resume.getId().equals(id))
			throw new IllegalArgumentException("Resume ID must be preserved");
		return resumeService.updateResume(resume);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Resume deleteResume(@PathVariable("id") long resumeId) {
		return resumeService.deleteResume(resumeId);
	}
}