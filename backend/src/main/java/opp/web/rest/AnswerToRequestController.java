package opp.web.rest;

import opp.domain.AnswerToRequest;
import opp.service.AnswerToRequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/answer-to-requests")
public class AnswerToRequestController {

	private final AnswerToRequestService answerToRequestService;

	public AnswerToRequestController(AnswerToRequestService answerToRequestService) {
		this.answerToRequestService = answerToRequestService;
	}

	@GetMapping
	public List<AnswerToRequest> listAnswerToRequests() {
		return answerToRequestService.listAll();
	}

	@GetMapping("/{id}")
	public AnswerToRequest getAnswerToRequest(@PathVariable("id") long id) {
		return answerToRequestService.fetch(id);
	}

	@PostMapping
	public ResponseEntity<AnswerToRequest> createAnswerToRequest(@RequestBody AnswerToRequest answerToRequest) {
		AnswerToRequest saved = answerToRequestService.createAnswerToRequest(answerToRequest);
		return ResponseEntity.created(URI.create(String.format("/answer-to-requests/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
	public AnswerToRequest updateAnswerToRequest(@PathVariable("id") Long id, @RequestBody AnswerToRequest answerToRequest) {
		if (!answerToRequest.getId().equals(id))
			throw new IllegalArgumentException("AnswerToRequest ID must be preserved");

		return answerToRequestService.updateAnswerToRequest(answerToRequest);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public AnswerToRequest deleteAnswerToRequest(@PathVariable("id") long answerToRequestId) {
		return answerToRequestService.deleteAnswerToRequest(answerToRequestId);
	}
}
