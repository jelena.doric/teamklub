package opp.web.rest;

import opp.domain.Qualification;
import opp.service.QualificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/qualifications")
public class QualificationController {

	private final QualificationService qualificationService;

	public QualificationController(QualificationService qualificationService) {
		this.qualificationService = qualificationService;
	}

	@GetMapping
	public List<Qualification> listQualifications() {
		return qualificationService.listAll();
	}

	@GetMapping("/{id}")
	public Qualification getQualification(@PathVariable("id") long id) {
		return qualificationService.fetch(id);
	}

	@PostMapping
//    @Secured("ROLE_ADMIN")
	public ResponseEntity<Qualification> createQualification(@RequestBody Qualification qualification) {
		Qualification saved = qualificationService.createQualification(qualification);
		return ResponseEntity.created(URI.create(String.format("/qualifications/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Qualification updateQualification(@PathVariable("id") Long id, @RequestBody Qualification qualification) {
		if (!qualification.getId().equals(id))
			throw new IllegalArgumentException("Qualification ID must be preserved");
		return qualificationService.updateQualification(qualification);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Qualification deleteQualification(@PathVariable("id") long qualificationId) {
		return qualificationService.deleteQualification(qualificationId);
	}
}
