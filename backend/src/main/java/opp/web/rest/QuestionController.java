package opp.web.rest;

import opp.domain.AnswerToQuestion;
import opp.domain.Question;
import opp.service.QuestionService;
import opp.web.dto.QuestionDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionController {

	private final QuestionService questionService;
	private final ModelMapper modelMapper;

	@Value("${opp.question.default-display-radius}")
	private Double defaultDisplayRadius;

	public QuestionController(QuestionService questionService, ModelMapper modelMapper) {
		this.questionService = questionService;
		this.modelMapper = modelMapper;
	}

	@GetMapping
	public List<Question> listQuestions() {
		return questionService.listAll();
	}

	@GetMapping("/{longitude}/{latitude}")
	public List<Question> listQuestions(@PathVariable("longitude") double longitude, @PathVariable("latitude") double latitude) {
	    return questionService.listAllInsideRadius(longitude, latitude);
	}

	@GetMapping("/{id}/answers")
	public List<AnswerToQuestion> listAnswers(@PathVariable("id") long id) {
		return questionService.listAnswers(id);
	}

	@GetMapping("/{id}")
	public Question getQuestion(@PathVariable("id") long id) {
		return questionService.fetch(id);
	}

	@PostMapping
	public ResponseEntity<Question> createQuestion(@RequestBody QuestionDTO questionDTO) {
		Question question = modelMapper.map(questionDTO, Question.class);
		question.setDisabled(false);
		question.setTimeCreated(new Date());
		question.setDisplayRadius(defaultDisplayRadius);

		Question saved = questionService.createQuestion(question);
		return ResponseEntity.created(URI.create(String.format("/questions/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
	public Question updateQuestion(@PathVariable("id") Long id, @RequestBody Question question) {
		if (!question.getId().equals(id))
			throw new IllegalArgumentException("Question ID must be preserved");
		return questionService.updateQuestion(question);
	}

	@PutMapping("/disable/{id}")
	public Question disableQuestion(@PathVariable("id") Long id) {
		return questionService.disableQuestion(id);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Question deleteQuestion(@PathVariable("id") long questionId) {
		return questionService.deleteQuestion(questionId);
	}
}
