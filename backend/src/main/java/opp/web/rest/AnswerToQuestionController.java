package opp.web.rest;

import opp.domain.AnswerToQuestion;
import opp.service.AnswerToQuestionService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/answer-to-questions")
public class AnswerToQuestionController {

	private final AnswerToQuestionService answerToQuestionService;

	public AnswerToQuestionController(AnswerToQuestionService answerToQuestionService) {
		this.answerToQuestionService = answerToQuestionService;
	}

	@GetMapping
	public List<AnswerToQuestion> listAnswerToQuestions() {
		return answerToQuestionService.listAll();
	}

	@GetMapping("/{id}")
	public AnswerToQuestion getAnswerToQuestion(@PathVariable("id") long id) {
		return answerToQuestionService.fetch(id);
	}

	@PostMapping
	public ResponseEntity<AnswerToQuestion> createAnswerToQuestion(@RequestBody AnswerToQuestion answerToQuestion) {
		AnswerToQuestion saved = answerToQuestionService.createAnswerToQuestion(answerToQuestion);
		return ResponseEntity.created(URI.create(String.format("/answer-to-questions/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
	public AnswerToQuestion updateAnswerToQuestion(@PathVariable("id") Long id, @RequestBody AnswerToQuestion answerToQuestion) {
		if (!answerToQuestion.getId().equals(id))
			throw new IllegalArgumentException("AnswerToQuestion ID must be preserved");
		return answerToQuestionService.updateAnswerToQuestion(answerToQuestion);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public AnswerToQuestion deleteAnswerToQuestion(@PathVariable("id") long answerToQuestionId) {
		return answerToQuestionService.deleteAnswerToQuestion(answerToQuestionId);
	}
}
