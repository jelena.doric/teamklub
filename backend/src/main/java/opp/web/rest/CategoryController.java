package opp.web.rest;

import opp.domain.Category;
import opp.service.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

	private final CategoryService categoryService;

	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping
	public List<Category> listCategories() {
		return categoryService.listAll();
	}

	@GetMapping("/{id}")
	public Category getCategory(@PathVariable("id") long id) {
		return categoryService.fetch(id);
	}

	@PostMapping
//    @Secured("ROLE_ADMIN")
	public ResponseEntity<Category> createCategory(@RequestBody Category category) {
		Category saved = categoryService.createCategory(category);
		return ResponseEntity.created(URI.create(String.format("/categories/%d", saved.getId()))).body(saved);
	}

	@PutMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Category updateCategory(@PathVariable("id") Long id, @RequestBody Category category) {
		if (!category.getId().equals(id))
			throw new IllegalArgumentException("Category ID must be preserved");
		return categoryService.updateCategory(category);
	}

	@DeleteMapping("/{id}")
//    @Secured("ROLE_ADMIN")
	public Category deleteCategory(@PathVariable("id") long categoryId) {
		return categoryService.deleteCategory(categoryId);
	}
}
