package opp.web.security.jwt;

import opp.domain.AppUser;

import java.io.Serializable;

public class JwtTokenResponse implements Serializable {

	private static final long serialVersionUID = 8317676219297719109L;

	private final AppUser user;

	private final String token;

	public JwtTokenResponse(String token, AppUser user) {
		this.token = token;
		this.user = user;
	}

	public AppUser getUser() {
		return user;
	}

	public String getToken() {
		return this.token;
	}
}