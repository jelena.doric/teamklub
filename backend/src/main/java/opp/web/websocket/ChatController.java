//package opp.web.websocket;
//
//import opp.web.websocket.Message;
//import opp.web.websocket.OutputMessage;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.SendTo;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//@Controller
//@RestController
//public class ChatController {
//
//	@MessageMapping("/chat")
//	@SendTo("/topic/messages")
//	public OutputMessage send(final Message message) throws Exception {
//		final String time = new SimpleDateFormat("HH:mm").format(new Date());
//		return new OutputMessage(message.getFrom(), message.getText(), time);
//	}
//
//}