package opp.service;

import opp.domain.Category;
import opp.exceptions.EntityMissingException;
import opp.exceptions.RequestDeniedException;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

	List<Category> listAll();

	/**
	 * Fetches Category with given ID.
	 *
	 * @param categoryId given Category ID
	 * @return Category associated with given ID in the system
	 * @throws EntityMissingException if Category with that ID is not found
	 * @see CategoryService#findById(long)
	 */
	Category fetch(long categoryId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new category in the system.
	 *
	 * @param category object to create, with ID set to null
	 * @return created category object in the system with ID set
	 * @throws IllegalArgumentException if given category is null, or its ID is NOT null
	 * @see Category
	 */
	Category createCategory(Category category);

	/**
	 * Finds Category with given ID, if exists.
	 *
	 * @param categoryId given Category ID
	 * @return Optional with value of Category associated with given ID in the system,
	 * or no value if one does not exist
	 * @see CategoryService#fetch
	 */
	Optional<Category> findById(long categoryId);

	/**
	 * Updates the category with that same ID.
	 *
	 * @param category object to update, with ID set
	 * @return updated category object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if category with given ID is not found
	 * @see CategoryService#createCategory(Category)
	 */
	Category updateCategory(Category category);

	/**
	 * Deletes one Category.
	 *
	 * @param categoryId ID of Category to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Category with that ID is not found
	 */
	Category deleteCategory(long categoryId);

}