package opp.service.impl;

import opp.dao.SkillRepository;
import opp.domain.Skill;
import opp.exceptions.EntityMissingException;
import opp.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class SkillServiceJpa implements SkillService {

	private final SkillRepository skillRepo;

	public SkillServiceJpa(SkillRepository skillRepo) {
		this.skillRepo = skillRepo;
	}

	@Override
	public List<Skill> listAll() {
		return skillRepo.findAll();
	}

	@Override
	public Optional<Skill> findById(long skillId) {
		return skillRepo.findById(skillId);
	}

	@Override
	public Optional<Skill> findBySkillName(String skillName) {
		return skillRepo.findBySkillName(skillName);
	}

	@Override
	public Skill fetch(long skillId) {
		return findById(skillId).orElseThrow(
				() -> new EntityMissingException(Skill.class, skillId)
		);
	}

	@Override
	public Skill createSkill(Skill skill) {
		validate(skill);
		Assert.isNull(skill.getId(),
				"Skill ID must be null, not: " + skill.getId()
		);

		return skillRepo.save(skill);
	}

	@Override
	public Skill updateSkill(Skill skill) {
		validate(skill);
		Long skillId = skill.getId();
		if (!skillRepo.existsById(skillId))
			throw new EntityMissingException(Skill.class, skillId);

		return skillRepo.save(skill);
	}

	@Override
	public Skill deleteSkill(long skillId) {
		Skill skill = fetch(skillId);
		skillRepo.delete(skill);
		return skill;
	}

	private void validate(Skill skill) {
		Assert.notNull(skill, "Skill object must be given");
	}
}
