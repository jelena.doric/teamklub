package opp.service.impl;

import opp.dao.RoleRepository;
import opp.domain.Role;
import opp.exceptions.EntityMissingException;
import opp.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceJpa implements RoleService {

	private final RoleRepository roleRepo;

	public RoleServiceJpa(RoleRepository roleRepo) {
		this.roleRepo = roleRepo;
	}

	@Override
	public List<Role> listAll() {
		return roleRepo.findAll();
	}

	@Override
	public Optional<Role> findById(long roleId) {
		return roleRepo.findById(roleId);
	}

	@Override
	public Role fetch(long roleId) {
		return findById(roleId).orElseThrow(() -> new EntityMissingException(Role.class, roleId));
	}

	@Override
	public Role createRole(Role role) {
		validate(role);
		Assert.isNull(role.getId(),
				"Role ID must be null, not: " + role.getId()
		);

		return roleRepo.save(role);
	}

	@Override
	public Role updateRole(Role role) {
		validate(role);
		Long roleId = role.getId();
		if (!roleRepo.existsById(roleId))
			throw new EntityMissingException(Role.class, roleId);

		return roleRepo.save(role);
	}

	@Override
	public Role deleteRole(long roleId) {
		Role role = fetch(roleId);
		roleRepo.delete(role);
		return role;
	}

	private void validate(Role role) {
		Assert.notNull(role, "Role object must be given");
	}
}
