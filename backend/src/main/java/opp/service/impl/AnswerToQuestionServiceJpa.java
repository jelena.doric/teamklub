package opp.service.impl;

import opp.dao.AnswerToQuestionRepository;
import opp.domain.AnswerToQuestion;
import opp.exceptions.EntityMissingException;
import opp.service.AnswerToQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerToQuestionServiceJpa implements AnswerToQuestionService {

	private final AnswerToQuestionRepository answerToQuestionRepo;

	public AnswerToQuestionServiceJpa(AnswerToQuestionRepository answerToQuestionRepo) {
		this.answerToQuestionRepo = answerToQuestionRepo;
	}

	@Override
	public List<AnswerToQuestion> listAll() {
		return answerToQuestionRepo.findAll();
	}

	@Override
	public Optional<AnswerToQuestion> findById(long answerToQuestionId) {
		return answerToQuestionRepo.findById(answerToQuestionId);
	}

	@Override
	public AnswerToQuestion fetch(long answerToQuestionId) {
		return findById(answerToQuestionId).orElseThrow(
				() -> new EntityMissingException(AnswerToQuestion.class, answerToQuestionId)
		);
	}

	@Override
	public AnswerToQuestion createAnswerToQuestion(AnswerToQuestion answerToQuestion) {
		validate(answerToQuestion);
		Assert.isNull(answerToQuestion.getId(),
				"AnswerToQuestion ID must be null, not: " + answerToQuestion.getId()
		);

		return answerToQuestionRepo.save(answerToQuestion);
	}

	@Override
	public AnswerToQuestion updateAnswerToQuestion(AnswerToQuestion answerToQuestion) {
		validate(answerToQuestion);
		Long answerToQuestionId = answerToQuestion.getId();
		if (!answerToQuestionRepo.existsById(answerToQuestionId))
			throw new EntityMissingException(AnswerToQuestion.class, answerToQuestionId);

		return answerToQuestionRepo.save(answerToQuestion);
	}

	@Override
	public AnswerToQuestion deleteAnswerToQuestion(long answerToQuestionId) {
		AnswerToQuestion answerToQuestion = fetch(answerToQuestionId);
		answerToQuestionRepo.delete(answerToQuestion);
		return answerToQuestion;
	}

	private void validate(AnswerToQuestion answerToQuestion) {
		Assert.notNull(answerToQuestion, "AnswerToQuestion object must be given");
	}
}
