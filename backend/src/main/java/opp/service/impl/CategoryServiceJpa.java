package opp.service.impl;

import opp.dao.CategoryRepository;
import opp.domain.Category;
import opp.exceptions.EntityMissingException;
import opp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceJpa implements CategoryService {

	private final CategoryRepository categoryRepo;

	public CategoryServiceJpa(CategoryRepository categoryRepo) {
		this.categoryRepo = categoryRepo;
	}

	@Override
	public List<Category> listAll() {
		return categoryRepo.findAll();
	}

	@Override
	public Optional<Category> findById(long categoryId) {
		return categoryRepo.findById(categoryId);
	}

	@Override
	public Category fetch(long categoryId) {
		return findById(categoryId).orElseThrow(
				() -> new EntityMissingException(Category.class, categoryId)
		);
	}

	@Override
	public Category createCategory(Category category) {
		validate(category);
		Assert.isNull(category.getId(),
				"Category ID must be null, not: " + category.getId()
		);

		return categoryRepo.save(category);
	}

	@Override
	public Category updateCategory(Category category) {
		validate(category);
		Long categoryId = category.getId();
		if (!categoryRepo.existsById(categoryId))
			throw new EntityMissingException(Category.class, categoryId);

		return categoryRepo.save(category);
	}

	@Override
	public Category deleteCategory(long categoryId) {
		Category category = fetch(categoryId);
		categoryRepo.delete(category);
		return category;
	}

	private void validate(Category category) {
		Assert.notNull(category, "Category object must be given");
	}
}
