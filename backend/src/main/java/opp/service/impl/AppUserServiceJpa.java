package opp.service.impl;

import opp.dao.*;
import opp.domain.*;
import opp.exceptions.EntityMissingException;
import opp.service.AppUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
//@EnableScheduling
public class AppUserServiceJpa implements AppUserService {

	private final AppUserRepository userRepo;
	private final RoleRepository roleRepo;
	private final QuestionRepository questionRepo;
	private final AnswerToQuestionRepository answerToQuestionRepo;
	private final RequestRepository requestRepo;
	private final AnswerToRequestRepository answerToRequestRepo;
	private final QualificationRepository qualificationRepo;
	private final SkillRepository skillRepo;

//	@Value("${opp.appusers.timeout-decrement}")
//	private Integer timeoutDecrement;

	@Value("${opp.appusers.timeout}")
	private Integer timeout;

	public AppUserServiceJpa(AppUserRepository userRepo,
							 RoleRepository roleRepo,
							 QuestionRepository questionRepo,
							 AnswerToQuestionRepository answerToQuestionRepo,
							 RequestRepository requestRepo,
							 AnswerToRequestRepository answerToRequestRepo,
							 QualificationRepository qualificationRepo,
							 SkillRepository skillRepo) {

		this.userRepo = userRepo;
		this.roleRepo = roleRepo;
		this.questionRepo = questionRepo;
		this.answerToQuestionRepo = answerToQuestionRepo;
		this.requestRepo = requestRepo;
		this.answerToRequestRepo = answerToRequestRepo;
		this.qualificationRepo = qualificationRepo;
		this.skillRepo = skillRepo;
	}

	@Override
	public List<AppUser> listAll() {
		return userRepo.findAll();
	}

	@Override
	public List<AppUser> listProfessionalUsers() {
		return userRepo.findAll().stream().filter(user -> user.getRole().getRoleName().equals("ROLE_PROFESSIONAL")).collect(Collectors.toList());
	}

	@Override
	public Optional<AppUser> findById(long userId) {
		return userRepo.findById(userId);
	}

	@Override
	public Optional<AppUser> findByUsername(String username) {
		return userRepo.findByUsername(username);
	}

	@Override
	public AppUser fetch(long userId) {
		return findById(userId).orElseThrow(
				() -> new EntityMissingException(AppUser.class, userId)
		);
	}

	@Override
	public AppUser createUser(AppUser user) {
		validate(user);
		Assert.isNull(user.getId(),
				"User ID must be null, not: " + user.getId()
		);

		changeRole(user, "ROLE_USER");

		return userRepo.save(user);
	}

	@Override
	public AppUser createOrUpdateProfessionalUser(AppUser user, Category category, List<Qualification> qualifications, List<Skill> skills) {
		validate(user);

		user.setQualifications(new HashSet<>(qualifications));
		user.setSkills(new HashSet<>(skills));
		user.setCategory(category);

		changeRole(user, "ROLE_PROFESSIONAL");

		return userRepo.save(user);
	}

	@Override
	public AppUser updateUser(AppUser user) {
		validate(user);
		Long userId = user.getId();
		if (!userRepo.existsById(userId))
			throw new EntityMissingException(AppUser.class, userId);

		return userRepo.save(user);
	}

	@Override
	public AppUser updateProfilePicture(long userId, String picturePath) {
		AppUser user = fetch(userId);

		user.setUserPicturePath(picturePath);

		return userRepo.save(user);
	}

	@Override
	public AppUser deleteUser(long userId) {
		AppUser user = fetch(userId);

		userRepo.delete(user);
		return user;
	}

	@Override
	public List<Question> listAskedQuestions(long id) {
		AppUser user = fetch(id);

		return questionRepo.findAll().stream().filter(question -> question.getUser().getId().equals(user.getId())).collect(Collectors.toList());
	}

	@Override
	public List<Question> listAnsweredQuestions(long id) {
		AppUser user = fetch(id);

		return answerToQuestionRepo.findAll().stream()
				.filter(answer -> answer.getUser().getId().equals(user.getId()))
				.map(AnswerToQuestion::getQuestion)
				.collect(Collectors.toList());
	}

	@Override
	public List<Request> listUserRequests(long id) {
		AppUser user = fetch(id);

		return requestRepo.findAll().stream().filter(request -> request.getUser().getId().equals(user.getId())).collect(Collectors.toList());
	}

	@Override
	public List<Request> listReceivedRequests(long id) {
		AppUser user = fetch(id);

		return answerToRequestRepo.findAll().stream()
				.map(AnswerToRequest::getRequest)
				.filter(request -> request.getProfessional().getId().equals(user.getId()))
				.collect(Collectors.toList());
	}


	@Override
	public AppUser grantAdminRights(long id) {
		AppUser user = fetch(id);

		changeRole(user, "ROLE_ADMIN");

		return userRepo.save(user);
	}

	@Override
	public AppUser giveRating(long id, double rating) {
		AppUser user = fetch(id);

		Double oldRating = user.getRating();

		if (oldRating == null) {
			user.setRating(rating);
			user.setNumberOfEngagements(1);
		} else {
			Integer numberOfEngagements = user.getNumberOfEngagements();
			user.setRating((oldRating * numberOfEngagements + rating) / (numberOfEngagements + 1));
		}

		return userRepo.save(user);
	}

	@Override
	public AppUser changeRole(AppUser user, String roleName) {
		Role newRole = roleRepo.findByRoleName(roleName).orElseThrow(
				() -> new EntityMissingException(Role.class, roleName)
		);

		user.setRole(newRole);

		return user;
	}

	@Override
	public AppUser banUser(long id) {
		AppUser user = fetch(id);

		user.setTimeout(timeout);

		return userRepo.save(user);
	}

	@Override
	public AppUser deactivateUser(long id) {
		AppUser user = fetch(id);

		user.setDeactivated(true);

		return userRepo.save(user);
	}

//	@Async
//	@Scheduled(initialDelay = 300000, fixedDelay = 300000)
//	public void decrementTimeout() {
//		userRepo.findAll().forEach(user -> {
//			Integer timeout = user.getTimeout();
//			if (timeout != null) {
//				int newTimeout = timeout - timeoutDecrement;
//				user.setTimeout(newTimeout <= 0 ? null : newTimeout);
//				userRepo.save(user);
//			}
//		});
//	}


	private void validate(AppUser user) {
		Assert.notNull(user, "User object must be given");
	}
}
