package opp.service.impl;

import opp.dao.AdminMessageRepository;
import opp.domain.AdminMessage;
import opp.exceptions.EntityMissingException;
import opp.service.AdmimMessageService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class AdminMessageServiceJpa implements AdmimMessageService {

	private final AdminMessageRepository messageRepo;

	public AdminMessageServiceJpa(AdminMessageRepository messageRepo) {
		this.messageRepo = messageRepo;
	}

	@Override
	public List<AdminMessage> listAll() {
		return messageRepo.findAll();
	}

	@Override
	public Optional<AdminMessage> findById(long messageId) {
		return messageRepo.findById(messageId);
	}

	@Override
	public AdminMessage fetch(long messageId) {
		return findById(messageId).orElseThrow(() -> new EntityMissingException(AdminMessage.class, messageId));
	}

	@Override
	public AdminMessage createMessage(AdminMessage message) {
		validate(message);
		Assert.isNull(message.getId(),
				"Message ID must be null, not: " + message.getId()
		);

		return messageRepo.save(message);
	}

	@Override
	public AdminMessage updateMessage(AdminMessage message) {
		validate(message);
		Long messageId = message.getId();
		if (!messageRepo.existsById(messageId))
			throw new EntityMissingException(AdminMessage.class, messageId);

		return messageRepo.save(message);
	}

	@Override
	public AdminMessage deleteMessage(long messageId) {
		AdminMessage message = fetch(messageId);
		messageRepo.delete(message);
		return message;
	}

	private void validate(AdminMessage message) {
		Assert.notNull(message, "Message object must be given");
	}
}