package opp.service.impl;

import opp.dao.ResumeRepository;
import opp.domain.Resume;
import opp.exceptions.EntityMissingException;
import opp.service.ResumeService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class ResumeServiceJpa implements ResumeService {

	private final ResumeRepository resumeRepo;

	public ResumeServiceJpa(ResumeRepository resumeRepo) {
		this.resumeRepo = resumeRepo;
	}

	@Override
	public List<Resume> listAll() {
		return resumeRepo.findAll();
	}

	@Override
	public Optional<Resume> findById(long resumeId) {
		return resumeRepo.findById(resumeId);
	}

	@Override
	public Resume fetch(long resumeId) {
		return findById(resumeId).orElseThrow(() -> new EntityMissingException(Resume.class, resumeId));
	}

	@Override
	public Resume createResume(Resume resume) {
		validate(resume);
		Assert.isNull(resume.getId(),
				"Resume ID must be null, not: " + resume.getId()
		);

		return resumeRepo.save(resume);
	}

	@Override
	public Resume updateResume(Resume resume) {
		validate(resume);
		Long resumeId = resume.getId();
		if (!resumeRepo.existsById(resumeId))
			throw new EntityMissingException(Resume.class, resumeId);

		return resumeRepo.save(resume);
	}

	@Override
	public Resume deleteResume(long resumeId) {
		Resume resume = fetch(resumeId);
		resumeRepo.delete(resume);
		return resume;
	}

	private void validate(Resume resume) {
		Assert.notNull(resume, "Resume object must be given");
	}
}