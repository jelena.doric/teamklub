package opp.service.impl;

import opp.domain.AppUser;
import opp.domain.Role;
import opp.service.AppUserService;
import opp.web.security.jwt.JwtUserDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Value("${opp.admin.password}")
	private String adminPasswordHash;

	@Value("${opp.admin.email}")
	private String adminEmail;


	private final AppUserService userService;
	private final BCryptPasswordEncoder passwordEncoder;

	public AppUserDetailsService(AppUserService userService, BCryptPasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser appUser = username.equals("admin") ?
				new AppUser(username, adminPasswordHash, adminEmail) :
				userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Username \"" + username + "\" not found"));

		if (appUser.getTimeout() != null  || (appUser.getDeactivated() != null && appUser.getDeactivated())) throw new AccessDeniedException(username);

		JwtUserDetails admin = new JwtUserDetails(username, username.equals("admin") ? adminPasswordHash : passwordEncoder.encode(appUser.getPassword()), authorities(appUser));
		admin.getUsername();

		return admin;
	}

	private List<GrantedAuthority> authorities(AppUser appUser) {
		List<GrantedAuthority> authorities = new ArrayList<>(commaSeparatedStringToAuthorityList("ROLE_USER"));

		if (appUser.getUsername().equals("admin")) {
			authorities.addAll(commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
		} else {
			if (appUser.getRole().getRoleName().contains("ROLE_ADMIN"))
				authorities.addAll(commaSeparatedStringToAuthorityList("ROLE_ADMIN"));

			if (appUser.getRole().getRoleName().contains("ROLE_PROFESSIONAL"))
				authorities.addAll(commaSeparatedStringToAuthorityList("ROLE_PROFESSIONAL"));
		}

		return authorities;
	}
}
