package opp.service.impl;

import opp.dao.AnswerToQuestionRepository;
import opp.dao.QuestionRepository;
import opp.domain.AnswerToQuestion;
import opp.domain.Question;
import opp.exceptions.EntityMissingException;
import opp.service.QuestionService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
//@EnableScheduling
public class QuestionServiceJpa implements QuestionService {

	private final QuestionRepository questionRepo;
	private final AnswerToQuestionRepository answerToQuestionRepo;

//	@Value("${opp.question.radius-increment}")
//	private Double radiusIncrement;

	public QuestionServiceJpa(QuestionRepository questionRepo, AnswerToQuestionRepository answerToQuestionRepo) {
		this.questionRepo = questionRepo;
		this.answerToQuestionRepo = answerToQuestionRepo;
	}

	@Override
	public List<Question> listAll() {
		return questionRepo.findAll();
	}

	@Override
	public List<Question> listAllInsideRadius(double longitude, double latitude) {
		return questionRepo.findAll().stream().filter(question -> isInRadius(question, longitude, latitude)).collect(Collectors.toList());
	}

	@Override
	public List<AnswerToQuestion> listAnswers(long id) {
		Question question = fetch(id);

		return answerToQuestionRepo.findAll().stream().filter(answer -> answer.getQuestion().getId().equals(question.getId())).collect(Collectors.toList());
	}

	@Override
	public Optional<Question> findById(long questionId) {
		return questionRepo.findById(questionId);
	}

	@Override
	public Question fetch(long questionId) {
		return findById(questionId).orElseThrow(
				() -> new EntityMissingException(Question.class, questionId)
		);
	}

	@Override
	public Question createQuestion(Question question) {
		validate(question);
		Assert.isNull(question.getId(),
				"Question ID must be null, not: " + question.getId()
		);

		return questionRepo.save(question);
	}

	@Override
	public Question updateQuestion(Question question) {
		validate(question);
		Long questionId = question.getId();
		if (!questionRepo.existsById(questionId))
			throw new EntityMissingException(Question.class, questionId);

		return questionRepo.save(question);
	}

	@Override
	public Question disableQuestion(long questionId) {
		Question question = fetch(questionId);

		question.setDisabled(true);

		return questionRepo.save(question);
	}

	@Override
	public Question deleteQuestion(long questionId) {
		Question question = fetch(questionId);

		listAnswers(questionId).forEach(answerToQuestionRepo::delete);
		questionRepo.delete(question);

		return question;
	}

//	@Async
//	@Scheduled(initialDelay = 300000, fixedDelay = 300000)
//	public void incrementDisplayRadius() {
//		questionRepo.findAll().forEach(question -> {
//			if (question.getDisplayRadius() != null) {
//				if (listAnswers(question.getId()).size() == 0) {
//					question.setDisplayRadius(question.getDisplayRadius() + radiusIncrement);
//					questionRepo.save(question);
//				}
//			}
//		});
//	}

	private void validate(Question question) {
		Assert.notNull(question, "Question object must be given");
	}

	private boolean isInRadius(Question question, double longitude, double latitude) {
		return Math.sqrt(Math.pow(question.getLongitude() - longitude, 2) + Math.pow(question.getLatitude() - latitude, 2)) <= question.getDisplayRadius();
	}
}
