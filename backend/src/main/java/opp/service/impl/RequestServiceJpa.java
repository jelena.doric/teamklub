package opp.service.impl;

import opp.dao.AnswerToRequestRepository;
import opp.dao.RequestRepository;
import opp.domain.AnswerToRequest;
import opp.domain.Request;
import opp.exceptions.EntityMissingException;
import opp.service.RequestService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RequestServiceJpa implements RequestService {

	private final RequestRepository requestRepo;
	private final AnswerToRequestRepository answerToRequestRepo;

	public RequestServiceJpa(RequestRepository requestRepo, AnswerToRequestRepository answerToRequestRepository) {
		this.requestRepo = requestRepo;
		this.answerToRequestRepo = answerToRequestRepository;
	}

	@Override
	public List<Request> listAll() {
		return requestRepo.findAll();
	}

	@Override
	public List<AnswerToRequest> listAnswers(long id) {
		Request question = fetch(id);

		return answerToRequestRepo.findAll().stream().filter(answer -> answer.getRequest().getId().equals(question.getId())).collect(Collectors.toList());
	}

	@Override
	public Optional<Request> findById(long requestId) {
		return requestRepo.findById(requestId);
	}

	@Override
	public Request fetch(long requestId) {
		return findById(requestId).orElseThrow(
				() -> new EntityMissingException(Request.class, requestId)
		);
	}

	@Override
	public Request createRequest(Request request) {
		validate(request);
		Assert.isNull(request.getId(),
				"Request ID must be null, not: " + request.getId()
		);

		return requestRepo.save(request);
	}

	@Override
	public Request updateRequest(Request request) {
		validate(request);
		Long requestId = request.getId();
		if (!requestRepo.existsById(requestId))
			throw new EntityMissingException(Request.class, requestId);

		return requestRepo.save(request);
	}

	@Override
	public Request deleteRequest(long requestId) {
		Request request = fetch(requestId);
		requestRepo.delete(request);
		return request;
	}

	private void validate(Request request) {
		Assert.notNull(request, "Request object must be given");
	}
}
