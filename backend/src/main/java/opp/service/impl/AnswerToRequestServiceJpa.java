package opp.service.impl;

import opp.dao.AnswerToRequestRepository;
import opp.domain.AnswerToRequest;
import opp.exceptions.EntityMissingException;
import opp.service.AnswerToRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerToRequestServiceJpa implements AnswerToRequestService {

	private final AnswerToRequestRepository answerToRequestRepo;

	public AnswerToRequestServiceJpa(AnswerToRequestRepository answerToRequestRepo) {
		this.answerToRequestRepo = answerToRequestRepo;
	}

	@Override
	public List<AnswerToRequest> listAll() {
		return answerToRequestRepo.findAll();
	}

	@Override
	public Optional<AnswerToRequest> findById(long answerToRequestId) {
		return answerToRequestRepo.findById(answerToRequestId);
	}

	@Override
	public AnswerToRequest fetch(long answerToRequestId) {
		return findById(answerToRequestId).orElseThrow(
				() -> new EntityMissingException(AnswerToRequest.class, answerToRequestId)
		);
	}

	@Override
	public AnswerToRequest createAnswerToRequest(AnswerToRequest answerToRequest) {
		validate(answerToRequest);
		Assert.isNull(answerToRequest.getId(),
				"AnswerToRequest ID must be null, not: " + answerToRequest.getId()
		);

		return answerToRequestRepo.save(answerToRequest);
	}

	@Override
	public AnswerToRequest updateAnswerToRequest(AnswerToRequest answerToRequest) {
		validate(answerToRequest);
		Long answerToRequestId = answerToRequest.getId();
		if (!answerToRequestRepo.existsById(answerToRequestId))
			throw new EntityMissingException(AnswerToRequest.class, answerToRequestId);

		return answerToRequestRepo.save(answerToRequest);
	}

	@Override
	public AnswerToRequest deleteAnswerToRequest(long answerToRequestId) {
		AnswerToRequest answerToRequest = fetch(answerToRequestId);
		answerToRequestRepo.delete(answerToRequest);
		return answerToRequest;
	}

	private void validate(AnswerToRequest answerToRequest) {
		Assert.notNull(answerToRequest, "AnswerToRequest object must be given");
	}
}
