package opp.service.impl;

import opp.dao.QualificationRepository;
import opp.domain.Qualification;
import opp.exceptions.EntityMissingException;
import opp.service.QualificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class QualificationServiceJpa implements QualificationService {

	private final QualificationRepository qualificationRepo;

	public QualificationServiceJpa(QualificationRepository qualificationRepo) {
		this.qualificationRepo = qualificationRepo;
	}

	@Override
	public List<Qualification> listAll() {
		return qualificationRepo.findAll();
	}

	@Override
	public Optional<Qualification> findById(long qualificationId) {
		return qualificationRepo.findById(qualificationId);
	}

	@Override
	public Optional<Qualification> findByQualificationName(String qualificationName) {
		return qualificationRepo.findByQualificationName(qualificationName);
	}

	@Override
	public Qualification fetch(long qualificationId) {
		return findById(qualificationId).orElseThrow(
				() -> new EntityMissingException(Qualification.class, qualificationId)
		);
	}

	@Override
	public Qualification createQualification(Qualification qualification) {
		validate(qualification);
		Assert.isNull(qualification.getId(),
				"Qualification ID must be null, not: " + qualification.getId()
		);

		return qualificationRepo.save(qualification);
	}

	@Override
	public Qualification updateQualification(Qualification qualification) {
		validate(qualification);
		Long qualificationId = qualification.getId();
		if (!qualificationRepo.existsById(qualificationId))
			throw new EntityMissingException(Qualification.class, qualificationId);

		return qualificationRepo.save(qualification);
	}

	@Override
	public Qualification deleteQualification(long qualificationId) {
		Qualification qualification = fetch(qualificationId);
		qualificationRepo.delete(qualification);
		return qualification;
	}

	private void validate(Qualification qualification) {
		Assert.notNull(qualification, "Qualification object must be given");
	}
}
