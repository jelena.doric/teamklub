package opp.service;

import opp.domain.AnswerToRequest;
import opp.exceptions.EntityMissingException;
import opp.exceptions.RequestDeniedException;

import java.util.List;
import java.util.Optional;

public interface AnswerToRequestService {

	List<AnswerToRequest> listAll();

	/**
	 * Fetches AnswerToRequest with given ID.
	 *
	 * @param answerToRequestId given AnswerToRequest ID
	 * @return AnswerToRequest associated with given ID in the system
	 * @throws EntityMissingException if AnswerToRequest with that ID is not found
	 * @see AnswerToRequestService#findById(long)
	 */
	AnswerToRequest fetch(long answerToRequestId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new answerToRequest in the system.
	 *
	 * @param answerToRequest object to create, with ID set to null
	 * @return created answerToRequest object in the system with ID set
	 * @throws IllegalArgumentException if given answerToRequest is null, or its ID is NOT null,
	 * @see AnswerToRequest
	 */
	AnswerToRequest createAnswerToRequest(AnswerToRequest answerToRequest);

	/**
	 * Finds AnswerToRequest with given ID, if exists.
	 *
	 * @param answerToRequestId given AnswerToRequest ID
	 * @return Optional with value of AnswerToRequest associated with given ID in the system,
	 * or no value if one does not exist
	 * @see AnswerToRequestService#fetch
	 */
	Optional<AnswerToRequest> findById(long answerToRequestId);

	/**
	 * Updates the answerToRequest with that same ID.
	 *
	 * @param answerToRequest object to update, with ID set
	 * @return updated answerToRequest object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if answerToRequest with given ID is not found
	 * @see AnswerToRequestService#createAnswerToRequest(AnswerToRequest)
	 */
	AnswerToRequest updateAnswerToRequest(AnswerToRequest answerToRequest);

	/**
	 * Deletes one AnswerToRequest.
	 *
	 * @param answerToRequestId ID of AnswerToRequest to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if AnswerToRequest with that ID is not found
	 */
	AnswerToRequest deleteAnswerToRequest(long answerToRequestId);

}