package opp.service;

import opp.domain.Resume;
import opp.exceptions.EntityMissingException;
import java.util.List;
import java.util.Optional;

public interface ResumeService {

	List<Resume> listAll();

	/**
	 * Fetches Resume with given ID.
	 *
	 * @param resumeId given Resume ID
	 * @return Resume associated with given ID in the system
	 * @throws EntityMissingException if Resume with that ID is not found
	 * @see ResumeService#findById(long)
	 */
	Resume fetch(long resumeId);

	/**
	 * Creates new resume in the system.
	 *
	 * @param resume object to create, with ID set to null
	 * @return created resume object in the system with ID set
	 * @throws IllegalArgumentException if given resume is null, or its ID is NOT null
	 * @see Resume
	 */
	Resume createResume(Resume resume);

	/**
	 * Finds Resume with given ID, if exists.
	 *
	 * @param resumeId given Resume ID
	 * @return Optional with value of Resume associated with given ID in the system,
	 * or no value if one does not exist
	 * @see ResumeService#fetch
	 */
	Optional<Resume> findById(long resumeId);

	/**
	 * Updates the resume with that same ID.
	 *
	 * @param resume object to update, with ID set
	 * @return updated resume object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if resume with given ID is not found
	 * @see ResumeService#createResume(Resume)
	 */
	Resume updateResume(Resume resume);

	/**
	 * Deletes one Resume.
	 *
	 * @param resumeId ID of Resume to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Resume with that ID is not found
	 */
	Resume deleteResume(long resumeId);
}