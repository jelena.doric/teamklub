package opp.service;

import opp.domain.AnswerToQuestion;
import opp.domain.Question;
import opp.exceptions.EntityMissingException;
import opp.exceptions.RequestDeniedException;

import java.util.List;
import java.util.Optional;

public interface QuestionService {

	List<Question> listAll();

	List<Question> listAllInsideRadius(double longitude, double latitude);

	List<AnswerToQuestion> listAnswers(long id);

	/**
	 * Fetches Question with given ID.
	 *
	 * @param questionId given Question ID
	 * @return Question associated with given ID in the system
	 * @throws EntityMissingException if Question with that ID is not found
	 * @see QuestionService#findById(long)
	 */
	Question fetch(long questionId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new question in the system.
	 *
	 * @param question object to create, with ID set to null
	 * @return created question object in the system with ID set
	 * @throws IllegalArgumentException if given question is null, or its ID is NOT null
	 * @see Question
	 */
	Question createQuestion(Question question);

	/**
	 * Finds Question with given ID, if exists.
	 *
	 * @param questionId given Question ID
	 * @return Optional with value of Question associated with given ID in the system,
	 * or no value if one does not exist
	 * @see QuestionService#fetch
	 */
	Optional<Question> findById(long questionId);

	/**
	 * Updates the question with that same ID.
	 *
	 * @param question object to update, with ID set
	 * @return updated question object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if question with given ID is not found
	 * @see QuestionService#createQuestion(Question)
	 */
	Question updateQuestion(Question question);

	/**
	 * Disables one Question.
	 *
	 * @param questionId ID of Question to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Question with that ID is not found
	 */
	Question disableQuestion(long questionId);

	/**
	 * Deletes one Question.
	 *
	 * @param questionId ID of Question to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Question with that ID is not found
	 */
	Question deleteQuestion(long questionId);

}