package opp.service;

import opp.domain.Role;
import opp.exceptions.EntityMissingException;

import java.util.List;
import java.util.Optional;

public interface RoleService {

	List<Role> listAll();

	/**
	 * Fetches Role with given ID.
	 *
	 * @param roleId given Role ID
	 * @return Role associated with given ID in the system
	 * @throws EntityMissingException if Role with that ID is not found
	 * @see RoleService#findById(long)
	 */
	Role fetch(long roleId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new role in the system.
	 *
	 * @param role object to create, with ID set to null
	 * @return created role object in the system with ID set
	 * @throws IllegalArgumentException if given role is null, or its ID is NOT null
	 * @see Role
	 */
	Role createRole(Role role);

	/**
	 * Finds Role with given ID, if exists.
	 *
	 * @param roleId given Role ID
	 * @return Optional with value of Role associated with given ID in the system,
	 * or no value if one does not exist
	 * @see RoleService#fetch
	 */
	Optional<Role> findById(long roleId);

	/**
	 * Updates the Role with that same ID.
	 *
	 * @param role object to update, with ID set
	 * @return updated Role object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if Role with given ID is not found
	 * @see RoleService#createRole(Role)
	 */
	Role updateRole(Role role);

	/**
	 * Deletes one Role.
	 *
	 * @param roleId of Role to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Role with that ID is not found
	 */
	Role deleteRole(long roleId);
}
