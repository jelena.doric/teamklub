package opp.service;

import opp.domain.AdminMessage;
import opp.exceptions.EntityMissingException;

import java.util.List;
import java.util.Optional;

public interface AdmimMessageService {

	List<AdminMessage> listAll();

	/**
	 * Fetches Message with given ID.
	 *
	 * @param messageId given Message ID
	 * @return Message associated with given ID in the system
	 * @throws EntityMissingException if Message with that ID is not found
	 * @see AdmimMessageService#findById(long)
	 */
	AdminMessage fetch(long messageId);

	/**
	 * Creates new message in the system.
	 *
	 * @param message object to create, with ID set to null
	 * @return created message object in the system with ID set
	 * @throws IllegalArgumentException if given message is null, or its ID is NOT null
	 * @see AdminMessage
	 */
	AdminMessage createMessage(AdminMessage message);

	/**
	 * Finds Message with given ID, if exists.
	 *
	 * @param messageId given Message ID
	 * @return Optional with value of Message associated with given ID in the system,
	 * or no value if one does not exist
	 * @see AdmimMessageService#fetch
	 */
	Optional<AdminMessage> findById(long messageId);

	/**
	 * Updates the message with that same ID.
	 *
	 * @param message object to update, with ID set
	 * @return updated message object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if message with given ID is not found
	 * @see AdmimMessageService#createMessage(AdminMessage)
	 */
	AdminMessage updateMessage(AdminMessage message);

	/**
	 * Deletes one Message.
	 *
	 * @param messageId ID of Message to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Message with that ID is not found
	 */
	AdminMessage deleteMessage(long messageId);

}
