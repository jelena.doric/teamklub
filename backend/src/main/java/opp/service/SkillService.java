package opp.service;

import opp.domain.Skill;
import opp.exceptions.EntityMissingException;
import opp.exceptions.RequestDeniedException;

import java.util.List;
import java.util.Optional;

public interface SkillService {

	List<Skill> listAll();

	/**
	 * Fetches Skill with given ID.
	 *
	 * @param skillId given Skill ID
	 * @return Skill associated with given ID in the system
	 * @throws EntityMissingException if Skill with that ID is not found
	 * @see SkillService#findById(long)
	 */
	Skill fetch(long skillId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new skill in the system.
	 *
	 * @param skill object to create, with ID set to null
	 * @return created skill object in the system with ID set
	 * @throws IllegalArgumentException if given skill is null, or its ID is NOT null
	 * @see Skill
	 */
	Skill createSkill(Skill skill);

	/**
	 * Finds Skill with given ID, if exists.
	 *
	 * @param skillId given Skill ID
	 * @return Optional with value of Skill associated with given ID in the system,
	 * or no value if one does not exist
	 * @see SkillService#fetch
	 */
	Optional<Skill> findById(long skillId);

	/**
	 * Finds the skill with given name.
	 *
	 * @param skillName skill name
	 * @return Optional with value of a skill associated with given name in the system,
	 * no value otherwise
	 * @throws IllegalArgumentException if given skillName is null
	 */
	Optional<Skill> findBySkillName(String skillName);

	/**
	 * Updates the skill with that same ID.
	 *
	 * @param skill object to update, with ID set
	 * @return updated skill object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if skill with given ID is not found
	 * @see SkillService#createSkill(Skill)
	 */
	Skill updateSkill(Skill skill);

	/**
	 * Deletes one Skill.
	 *
	 * @param skillId ID of Skill to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Skill with that ID is not found
	 */
	Skill deleteSkill(long skillId);
}