package opp.service;

import opp.domain.AnswerToQuestion;
import opp.exceptions.EntityMissingException;
import opp.exceptions.RequestDeniedException;

import java.util.List;
import java.util.Optional;

public interface AnswerToQuestionService {

	List<AnswerToQuestion> listAll();

	/**
	 * Fetches AnswerToQuestion with given ID.
	 *
	 * @param answerToQuestionId given AnswerToQuestion ID
	 * @return AnswerToQuestion associated with given ID in the system
	 * @throws EntityMissingException if AnswerToQuestion with that ID is not found
	 * @see AnswerToQuestionService#findById(long)
	 */
	AnswerToQuestion fetch(long answerToQuestionId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new answerToQuestion in the system.
	 *
	 * @param answerToQuestion object to create, with ID set to null
	 * @return created answerToQuestion object in the system with ID set
	 * @throws IllegalArgumentException if given answerToQuestion is null, or its ID is NOT null,
	 *                                  or its JMBAG is null or invalid
	 * @throws RequestDeniedException   if answerToQuestion with that JMBAG already exists in the system
	 * @see AnswerToQuestion
	 */
	AnswerToQuestion createAnswerToQuestion(AnswerToQuestion answerToQuestion);

	/**
	 * Finds AnswerToQuestion with given ID, if exists.
	 *
	 * @param answerToQuestionId given AnswerToQuestion ID
	 * @return Optional with value of AnswerToQuestion associated with given ID in the system,
	 * or no value if one does not exist
	 * @see AnswerToQuestionService#fetch
	 */
	Optional<AnswerToQuestion> findById(long answerToQuestionId);

	/**
	 * Updates the answerToQuestion with that same ID.
	 *
	 * @param answerToQuestion object to update, with ID set
	 * @return updated answerToQuestion object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID, or has null or invalid JMBAG
	 * @throws EntityMissingException   if answerToQuestion with given ID is not found
	 * @throws RequestDeniedException   if another answerToQuestion with some other ID and the same JMBAG already exists
	 * @see AnswerToQuestionService#createAnswerToQuestion(AnswerToQuestion)
	 */
	AnswerToQuestion updateAnswerToQuestion(AnswerToQuestion answerToQuestion);

	/**
	 * Deletes one AnswerToQuestion.
	 *
	 * @param answerToQuestionId ID of AnswerToQuestion to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if AnswerToQuestion with that ID is not found
	 */
	AnswerToQuestion deleteAnswerToQuestion(long answerToQuestionId);

}