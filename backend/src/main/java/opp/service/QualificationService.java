package opp.service;

import opp.domain.Qualification;
import opp.exceptions.EntityMissingException;
import opp.exceptions.RequestDeniedException;

import java.util.List;
import java.util.Optional;

public interface QualificationService {

	List<Qualification> listAll();

	/**
	 * Fetches Qualification with given ID.
	 *
	 * @param qualificationId given Qualification ID
	 * @return Qualification associated with given ID in the system
	 * @throws EntityMissingException if Qualification with that ID is not found
	 * @see QualificationService#findById(long)
	 */
	Qualification fetch(long qualificationId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new qualification in the system.
	 *
	 * @param qualification object to create, with ID set to null
	 * @return created qualification object in the system with ID set
	 * @throws IllegalArgumentException if given qualification is null, or its ID is NOT null
	 * @see Qualification
	 */
	Qualification createQualification(Qualification qualification);

	/**
	 * Finds Qualification with given ID, if exists.
	 *
	 * @param qualificationId given Qualification ID
	 * @return Optional with value of Qualification associated with given ID in the system,
	 * or no value if one does not exist
	 * @see QualificationService#fetch
	 */
	Optional<Qualification> findById(long qualificationId);

	/**
	 * Finds the qualification with given name.
	 *
	 * @param qualificationName qualification name
	 * @return Optional with value of a qualification associated with given name in the system,
	 * no value otherwise
	 * @throws IllegalArgumentException if given qualificationName is null
	 */
	Optional<Qualification> findByQualificationName(String qualificationName);

	/**
	 * Updates the qualification with that same ID.
	 *
	 * @param qualification object to update, with ID set
	 * @return updated qualification object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if qualification with given ID is not found
	 * @see QualificationService#createQualification(Qualification)
	 */
	Qualification updateQualification(Qualification qualification);

	/**
	 * Deletes one Qualification.
	 *
	 * @param qualificationId ID of Qualification to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Qualification with that ID is not found
	 */
	Qualification deleteQualification(long qualificationId);

}