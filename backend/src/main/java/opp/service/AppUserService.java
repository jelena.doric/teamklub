package opp.service;

import opp.domain.*;
import opp.exceptions.EntityMissingException;

import java.util.List;
import java.util.Optional;

public interface AppUserService {

	List<AppUser> listAll();

	List<AppUser> listProfessionalUsers();

	/**
	 * Fetches User with given ID.
	 *
	 * @param userId given User ID
	 * @return User associated with given ID in the system
	 * @throws EntityMissingException if User with that ID is not found
	 * @see AppUserService#findById(long)
	 */
	AppUser fetch(long userId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new user in the system.
	 *
	 * @param user object to create, with ID set to null
	 * @return created user object in the system with ID set
	 * @throws IllegalArgumentException if given user is null, or its ID is NOT null
	 * @see AppUser
	 */
	AppUser createUser(AppUser user);

	/**
	 * Finds User with given ID, if exists.
	 *
	 * @param userId given User ID
	 * @return Optional with value of User associated with given ID in the system,
	 * or no value if one does not exist
	 * @see AppUserService#fetch
	 */
	Optional<AppUser> findById(long userId);

	/**
	 * Finds the user with given username.
	 *
	 * @param username user username
	 * @return Optional with value of a user associated with given username exists in the system,
	 * no value otherwise
	 * @throws IllegalArgumentException if given username is null
	 */
	Optional<AppUser> findByUsername(String username);


	/**
	 * Updates the User with that same ID.
	 *
	 * @param user object to update, with ID set
	 * @return updated User object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if User with given ID is not found
	 * @see AppUserService#createUser(AppUser)
	 */
	AppUser updateUser(AppUser user);

	/**
	 * Updates the profile picture of the User with that ID.
	 *
	 * @param user object to update, with ID set
	 * @return updated User object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if User with given ID is not found
	 * @see AppUserService#createUser(AppUser)
	 */
	AppUser updateProfilePicture(long userId, String picturePath);

	/**
	 * Deletes one User.
	 *
	 * @param userId of User to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if User with that ID is not found
	 */
	AppUser deleteUser(long userId);

	/**
	 * Adds role to User.
	 *
	 * @param user     User to add role
	 * @param roleName of Role to add to user
	 * @return updated User
	 * @throws EntityMissingException if User with that username is not found or if Role with that roleName is not found
	 */
	AppUser changeRole(AppUser user, String roleName);

	/**
	 * Lists questions asked by the User.
	 *
	 * @param id of User
	 * @return List of Questions asked by the User
	 * @throws EntityMissingException if User with given ID is not found
	 */
	List<Question> listAskedQuestions(long id);

	/**
	 * Lists questions answered by the User.
	 *
	 * @param id of User
	 * @return List of Questions answered by the User
	 * @throws EntityMissingException if User with given ID is not found
	 */
	List<Question> listAnsweredQuestions(long id);

	/**
	 * Lists requests set by the User.
	 *
	 * @param id of User to add role
	 * @return List of Requests set by the User
	 * @throws EntityMissingException if User with given ID is not found
	 */
	List<Request> listUserRequests(long id);

	/**
	 * Lists requests requests answered by the User.
	 *
	 * @param id of User to add role
	 * @return List of AnswerToRequests answered by the Professional
	 * @throws EntityMissingException if User with given ID is not found
	 */
	List<Request> listReceivedRequests(long id);

	/**
	 * Grants admin rights to the User with given ID.
	 *
	 * @param id of User to grant admin rights to
	 * @return updated User object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if User with given ID is not found
	 */
	AppUser grantAdminRights(long id);

	/**
	 * Gives a new rating to professional user
	 *
	 * @param id of User to give rating to to
	 * @return updated User object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if User with given ID is not found
	 */
	AppUser giveRating(long id, double rating);

	/**
	 * Bans one User.
	 *
	 * @param userId of User to ban from the system
	 * @return banned user
	 * @throws EntityMissingException if User with that ID is not found
	 */
	AppUser banUser(long userId);

	/**
	 * Deactivates one User.
	 *
	 * @param userId of User to deactive
	 * @return deactivated user
	 * @throws EntityMissingException if User with that ID is not found
	 */
	AppUser deactivateUser(long userId);

	/**
	 * Creates a professionalUser from given regular user.
	 *
	 * @param user           User to create as a professional
	 * @param category
	 * @param qualifications qualification list of professional
	 * @param skills         skills list of professional
	 * @return updated User object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if User with given ID is not found
	 */
	AppUser createOrUpdateProfessionalUser(AppUser user, Category category, List<Qualification> qualifications, List<Skill> skills);
}
