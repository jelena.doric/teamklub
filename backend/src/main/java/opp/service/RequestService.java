package opp.service;

import opp.domain.AnswerToRequest;
import opp.domain.Request;
import opp.exceptions.EntityMissingException;
import opp.exceptions.RequestDeniedException;

import java.util.List;
import java.util.Optional;

public interface RequestService {

	List<Request> listAll();

	List<AnswerToRequest> listAnswers(long id);

	/**
	 * Fetches Request with given ID.
	 *
	 * @param requestId given Request ID
	 * @return Request associated with given ID in the system
	 * @throws EntityMissingException if Request with that ID is not found
	 * @see RequestService#findById(long)
	 */
	Request fetch(long requestId);
	// Note: verb "fetch" in method name is typically used when identified object is expected

	/**
	 * Creates new request in the system.
	 *
	 * @param request object to create, with ID set to null
	 * @return created request object in the system with ID set
	 * @throws IllegalArgumentException if given request is null, or its ID is NOT null
	 *                                  or its JMBAG is null or invalid
	 * @see Request
	 */
	Request createRequest(Request request);

	/**
	 * Finds Request with given ID, if exists.
	 *
	 * @param requestId given Request ID
	 * @return Optional with value of Request associated with given ID in the system,
	 * or no value if one does not exist
	 * @see RequestService#fetch
	 */
	Optional<Request> findById(long requestId);

	/**
	 * Updates the request with that same ID.
	 *
	 * @param request object to update, with ID set
	 * @return updated request object in the system
	 * @throws IllegalArgumentException if given object is null, has null ID
	 * @throws EntityMissingException   if request with given ID is not found
	 * @see RequestService#createRequest(Request)
	 */
	Request updateRequest(Request request);

	/**
	 * Deletes one Request.
	 *
	 * @param requestId ID of Request to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if Request with that ID is not found
	 */
	Request deleteRequest(long requestId);

}