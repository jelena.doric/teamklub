package opp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Request {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private AppUser professional;

	@OneToOne
	private AppUser user;

	private String requestMessage;

	private String requestPicturePath;

	@OneToOne
	private Category category;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AppUser getProfessional() {
		return professional;
	}

	public void setProfessional(AppUser professional) {
		this.professional = professional;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public String getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}

	public String getRequestPicturePath() {
		return requestPicturePath;
	}

	public void setRequestPicturePath(String requestPicturePath) {
		this.requestPicturePath = requestPicturePath;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
}
