package opp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class AnswerToQuestion {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private Question question;

	@OneToOne
	private AppUser user;

	private String answerJPGPath;
	private String answerTXT;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public String getAnswerJPGPath() {
		return answerJPGPath;
	}

	public void setAnswerJPGPath(String answerJPGPath) {
		this.answerJPGPath = answerJPGPath;
	}

	public String getAnswerTXT() {
		return answerTXT;
	}

	public void setAnswerTXT(String answerTXT) {
		this.answerTXT = answerTXT;
	}
}
