package opp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class Question {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private AppUser user;

	private String questionText;

	private String questionPicturePath;

	@OneToOne
	private Category category;

	private boolean disabled;

	private Double longitude;
	private Double latitude;

	private Double displayRadius;

	private Date timeCreated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getQuestionPicturePath() {
		return questionPicturePath;
	}

	public void setQuestionPicturePath(String questionPicturePath) {
		this.questionPicturePath = questionPicturePath;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getDisplayRadius() {
		return displayRadius;
	}

	public void setDisplayRadius(Double displayRadius) {
		this.displayRadius = displayRadius;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
}
