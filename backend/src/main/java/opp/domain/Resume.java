package opp.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Resume {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private AppUser user;

	@OneToOne
	private Category category;

	@OneToMany
	private List<Qualification> qualifications;

	@OneToMany
	private List<Skill> skills;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<Qualification> getQualifications() {
		return qualifications;
	}

	public void setQualifications(List<Qualification> qualifications) {
		this.qualifications = qualifications;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
}
