package opp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class AnswerToRequest {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne
	private Request request;

	private String answerJpgPath;
	private String answerTxt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public String getAnswerJpgPath() {
		return answerJpgPath;
	}

	public void setAnswerJpgPath(String answerJpgPath) {
		this.answerJpgPath = answerJpgPath;
	}

	public String getAnswerTxt() {
		return answerTxt;
	}

	public void setAnswerTxt(String answerTxt) {
		this.answerTxt = answerTxt;
	}
}
