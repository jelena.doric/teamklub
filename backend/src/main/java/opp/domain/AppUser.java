package opp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class AppUser {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true, nullable = false)
	@Size(min = 5, max = 15)
	private String username;

	@NotNull
	@Size(min = 5, max = 50)
	private String password;

	@Column(unique = true, nullable = false)
	@Size(min = 5, max = 50)
	private String email;

	private String firstName;
	private String lastName;

	private String userPicturePath;

	@OneToOne
	private Role role;

	@OneToOne
	private Category category;

	@OneToMany(fetch = FetchType.EAGER)
	private Set<Skill> skills;

	@OneToMany(fetch = FetchType.EAGER)
	private Set<Qualification> qualifications;

	private Integer numberOfEngagements;
	private Double rating;

	private Integer timeout;

	private Boolean deactivated;

	public AppUser() {
	}

	public AppUser(String username) {
		this.username = username;
	}

	public AppUser(String username, String password, String email) {
		this(username);
		this.password = password;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserPicturePath() {
		return userPicturePath;
	}

	public void setUserPicturePath(String userPicturePath) {
		this.userPicturePath = userPicturePath;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Set<Skill> getSkills() {
		return skills;
	}

	public void setSkills(Set<Skill> skills) {
		this.skills = skills;
	}

	public Set<Qualification> getQualifications() {
		return qualifications;
	}

	public void setQualifications(Set<Qualification> qualifications) {
		this.qualifications = qualifications;
	}

	public Integer getNumberOfEngagements() {
		return numberOfEngagements;
	}

	public void setNumberOfEngagements(Integer numberOfEngagements) {
		this.numberOfEngagements = numberOfEngagements;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Boolean getDeactivated() {
		return deactivated;
	}

	public void setDeactivated(Boolean deactivated) {
		this.deactivated = deactivated;
	}
}
