package opp.dao;

import opp.domain.AnswerToRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AnswerToRequestRepository extends JpaRepository<AnswerToRequest, Long> {
}
