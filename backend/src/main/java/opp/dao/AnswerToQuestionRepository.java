package opp.dao;

import opp.domain.AnswerToQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AnswerToQuestionRepository extends JpaRepository<AnswerToQuestion, Long> {
}
