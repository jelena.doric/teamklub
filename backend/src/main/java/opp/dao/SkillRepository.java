package opp.dao;

import opp.domain.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SkillRepository extends JpaRepository<Skill, Long> {

	Optional<Skill> findBySkillName(String skillName);
}
