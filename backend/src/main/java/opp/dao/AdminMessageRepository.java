package opp.dao;

import opp.domain.AdminMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminMessageRepository extends JpaRepository<AdminMessage, Long> {
}
